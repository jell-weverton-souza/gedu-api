package com.manager.gedu.interfaces;

import com.manager.gedu.abstracts.AbstractDomain;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Weverton Souza.
 * Created on 16/06/19
 */
@Repository
public interface IRepository<E extends AbstractDomain, K> extends MongoRepository<E, K> {
}
