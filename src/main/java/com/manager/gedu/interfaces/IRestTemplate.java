package com.manager.gedu.interfaces;


import com.manager.gedu.infrastructure.client.IBGE;
import com.manager.gedu.infrastructure.client.RestCountries;
import com.manager.gedu.infrastructure.client.ViaCepClient;

/**
 * @author Weverton Souza.
 * Created on 27/06/19
 */
public interface IRestTemplate {
    ViaCepClient getViaCepClient();
    RestCountries getRestCountriesClient();
    IBGE getRestIBGEClient();
}
