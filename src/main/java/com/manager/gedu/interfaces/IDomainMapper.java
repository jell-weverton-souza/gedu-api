package com.manager.gedu.interfaces;

import java.util.List;

/**
 * @author Weverton Souza.
 * Created on 13/07/19
 */
public interface IDomainMapper<E, D> {
    D toDTO(final E domain);
    E toDomain(final D dto);
    List<D> toPageDTO(final List<E> items);
}
