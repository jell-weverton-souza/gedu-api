package com.manager.gedu.interfaces;

/**
 * @author Weverton Souza.
 * Created on 27/06/19
 */
public interface IClient {
    public IClientDomain get();
}
