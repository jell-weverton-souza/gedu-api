package com.manager.gedu.interfaces;

import java.io.Serializable;
import java.util.UUID;

/**
 * @author Weverton Souza.
 * Created on 16/06/19
 */
public interface IDomain extends Serializable {
    Object setId(final String id);
    String getId();
}

