package com.manager.gedu.interfaces;

import java.io.Serializable;
import java.util.UUID;

/**
 * @author Weverton Souza.
 * Created on 30/06/19
 */
public interface IDataTransferObject extends Serializable {
    Object setId(final String id);
    String getId();
}
