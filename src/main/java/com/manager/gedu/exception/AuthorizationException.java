package com.manager.gedu.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author Weverton Souza.
 * Created on 02/07/19
 */
@ResponseStatus(HttpStatus.UNAUTHORIZED)
public class AuthorizationException extends RuntimeException {

    public AuthorizationException(final String msg) {
        super(msg);
    }

    public AuthorizationException(final String msg, final Throwable throwable) {
        super(msg, throwable);
    }
}
