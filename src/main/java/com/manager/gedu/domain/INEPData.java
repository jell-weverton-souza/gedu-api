package com.manager.gedu.domain;

import com.manager.gedu.abstracts.AbstractDomain;

/**
 * @author Weverton Souza.
 * Created on 01/08/2019
 */
public class INEPData extends AbstractDomain {
    private String codeRegister;
    private String codeUnit;
    private String codeClassUnit;
    private String codeStudentUnit;
    private String codeProfessionalUnit;

    public INEPData() {}

    public String getCodeRegister() {
        return codeRegister;
    }

    public void setCodeRegister(String codeRegister) {
        this.codeRegister = codeRegister;
    }

    public String getCodeUnit() {
        return codeUnit;
    }

    public void setCodeUnit(String codeUnit) {
        this.codeUnit = codeUnit;
    }

    public String getCodeClassUnit() {
        return codeClassUnit;
    }

    public void setCodeClassUnit(String codeClassUnit) {
        this.codeClassUnit = codeClassUnit;
    }

    public String getCodeStudentUnit() {
        return codeStudentUnit;
    }

    public void setCodeStudentUnit(String codeStudentUnit) {
        this.codeStudentUnit = codeStudentUnit;
    }

    public String getCodeProfessionalUnit() {
        return codeProfessionalUnit;
    }

    public void setCodeProfessionalUnit(String codeProfessionalUnit) {
        this.codeProfessionalUnit = codeProfessionalUnit;
    }
}
