package com.manager.gedu.domain.student;

import com.manager.gedu.abstracts.AbstractDomain;
import com.manager.gedu.domain.gradesubject.Grade;
import com.manager.gedu.domain.task.Score;
import com.manager.gedu.domain.user.User;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author Weverton Souza.
 * Created on 01/08/2019
 */
@Document("student")
public class Student extends AbstractDomain {
    @DBRef
    private User userData;
    @DBRef
    private Grade currentGrade;
    @DBRef
    private Score score;

    public Student() { }

    public User getUserData() {
        return userData;
    }

    public Student setUserData(final User userData) {
        this.userData = userData;
        return this;
    }

    public Grade getCurrentGrade() {
        return currentGrade;
    }

    public Student setCurrentGrade(final Grade currentGrade) {
        this.currentGrade = currentGrade;
        return this;
    }

    public Score getScore() {
        return score;
    }

    public Student setScore(final Score score) {
        this.score = score;
        return this;
    }
}
