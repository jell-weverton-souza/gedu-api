package com.manager.gedu.domain.unit;

import com.manager.gedu.abstracts.AbstractDomain;
import com.manager.gedu.domain.address.Address;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/**
 * @author Weverton Souza.
 * Created on 01/08/2019
 */
@Document(value = "unit")
public class Unit extends AbstractDomain {
    private String name;
    private String abbreviation;
    private String description;
    private List<String> observations;
    @DBRef
    private Address address;

    public Unit() {}

    public String getName() {
        return name;
    }

    public Unit setName(String name) {
        this.name = name;
        return this;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public Unit setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Unit setDescription(String description) {
        this.description = description;
        return this;
    }

    public List<String> getObservations() {
        return observations;
    }

    public Unit setObservations(List<String> observations) {
        this.observations = observations;
        return this;
    }

    public Address getAddress() {
        return address;
    }

    public Unit setAddress(Address address) {
        this.address = address;
        return this;
    }
}
