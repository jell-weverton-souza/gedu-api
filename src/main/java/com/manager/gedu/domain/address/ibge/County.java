package com.manager.gedu.domain.address.ibge;

import com.google.gson.annotations.SerializedName;
import com.manager.gedu.interfaces.IClientDomain;

public class County implements IClientDomain {
    private String id;
    @SerializedName(value = "nome")
    private String name;
    @SerializedName(value = "microrregiao")
    private Microregion microregion;

    public County() {}

    public String getId() {
        return id;
    }

    public County setId(String id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public County setName(String name) {
        this.name = name;
        return this;
    }

    public Microregion getMicroregion() {
        return microregion;
    }

    public County setMicroregion(Microregion microregion) {
        this.microregion = microregion;
        return this;
    }
}
