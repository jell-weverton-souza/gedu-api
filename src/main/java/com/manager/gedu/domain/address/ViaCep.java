package com.manager.gedu.domain.address;

import com.google.gson.annotations.SerializedName;
import com.manager.gedu.interfaces.IClientDomain;

/**
 * @author Weverton Souza.
 * Created on 27/06/19
 */
public class ViaCep implements IClientDomain {
    @SerializedName(value="cep")
    private String zipCode;
    @SerializedName(value="logradouro")
    private String publicPlace;
    @SerializedName(value="complemento")
    private String complement;
    @SerializedName(value="bairro")
    private String neighborhood;
    @SerializedName(value="localidade")
    private String city;
    @SerializedName(value="uf")
    private String state;
    @SerializedName(value="unidade")
    private String unit;
    private String ibge;
    private String gia;

    public ViaCep() { }

    public String getZipCode() {
        return zipCode;
    }

    public ViaCep setZipCode(String zipCode) {
        this.zipCode = zipCode;
        return this;
    }

    public String getPublicPlace() {
        return publicPlace;
    }

    public ViaCep setPublicPlace(String publicPlace) {
        this.publicPlace = publicPlace;
        return this;
    }

    public String getComplement() {
        return complement;
    }

    public ViaCep setComplement(String complement) {
        this.complement = complement;
        return this;
    }

    public String getNeighborhood() {
        return neighborhood;
    }

    public ViaCep setNeighborhood(String neighborhood) {
        this.neighborhood = neighborhood;
        return this;
    }

    public String getCity() {
        return city;
    }

    public ViaCep setCity(String city) {
        this.city = city;
        return this;
    }

    public String getState() {
        return state;
    }

    public ViaCep setState(String state) {
        this.state = state;
        return this;
    }

    public String getUnit() {
        return unit;
    }

    public ViaCep setUnit(String unit) {
        this.unit = unit;
        return this;
    }

    public String getIbge() {
        return ibge;
    }

    public ViaCep setIbge(String ibge) {
        this.ibge = ibge;
        return this;
    }

    public String getGia() {
        return gia;
    }

    public ViaCep setGia(String gia) {
        this.gia = gia;
        return this;
    }
}