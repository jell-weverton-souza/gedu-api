package com.manager.gedu.domain.address.ibge;

import com.google.gson.annotations.SerializedName;
import com.manager.gedu.interfaces.IClientDomain;

public class Region implements IClientDomain {
    private Integer id;
    @SerializedName(value = "sigla")
    private String initials;
    @SerializedName(value = "nome")
    private String name;

    public Region() {}

    public Integer getId() {
        return id;
    }

    public Region setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getInitials() {
        return initials;
    }

    public Region setInitials(String initials) {
        this.initials = initials;
        return this;
    }

    public String getName() {
        return name;
    }

    public Region setName(String name) {
        this.name = name;
        return this;
    }
}
