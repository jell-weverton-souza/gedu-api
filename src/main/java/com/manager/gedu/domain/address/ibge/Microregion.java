package com.manager.gedu.domain.address.ibge;

import com.google.gson.annotations.SerializedName;
import com.manager.gedu.interfaces.IClientDomain;

public class Microregion implements IClientDomain {
    private String id;
    @SerializedName(value = "nome")
    private String name;
    @SerializedName(value = "mesorregiao")
    private Mesoregion mesoregion;

    private Microregion() {}

    public String getId() {
        return id;
    }

    public Microregion setId(String id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Microregion setName(String name) {
        this.name = name;
        return this;
    }

    public Mesoregion getMesoregion() {
        return mesoregion;
    }

    public Microregion setMesoregion(Mesoregion mesoregion) {
        this.mesoregion = mesoregion;
        return this;
    }
}
