package com.manager.gedu.domain.address.ibge;

import com.google.gson.annotations.SerializedName;
import com.manager.gedu.interfaces.IClientDomain;

public class Mesoregion implements IClientDomain {
    private String id;
    @SerializedName(value = "nome")
    private String name;
    @SerializedName(value = "UF")
    private UF uf;

    public Mesoregion() {}

    public String getId() {
        return id;
    }

    public Mesoregion setId(String id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Mesoregion setName(String name) {
        this.name = name;
        return this;
    }

    public UF getUf() {
        return uf;
    }

    public Mesoregion setUf(UF uf) {
        this.uf = uf;
        return this;
    }
}
