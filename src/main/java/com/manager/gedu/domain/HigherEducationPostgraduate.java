package com.manager.gedu.domain;

import com.manager.gedu.abstracts.AbstractDomain;
import org.springframework.data.mongodb.core.mapping.Document;

import java.sql.Date;

/**
 * @author Weverton Souza.
 * Created on 01/08/2019
 */
@Document
public class HigherEducationPostgraduate extends AbstractDomain {
    private Integer courseLevel;
    private Integer courseType; // Criar tabela de tipos de cursos. Superior, Mestrado, Doutorado, etc.
    private Boolean courseSituation;
    private Boolean pedagogicalTrainingComplementarity;
    private String codeCourse;
    private Date startYear;
    private Date endYear;
    private String instituteName;
    private String codeUniversityINEP;

    public HigherEducationPostgraduate() {}

    public Integer getCourseLevel() {
        return courseLevel;
    }

    public void setCourseLevel(Integer courseLevel) {
        this.courseLevel = courseLevel;
    }

    public Integer getCourseType() {
        return courseType;
    }

    public void setCourseType(Integer courseType) {
        this.courseType = courseType;
    }

    public Boolean getCourseSituation() {
        return courseSituation;
    }

    public void setCourseSituation(Boolean courseSituation) {
        this.courseSituation = courseSituation;
    }

    public Boolean getPedagogicalTrainingComplementarity() {
        return pedagogicalTrainingComplementarity;
    }

    public void setPedagogicalTrainingComplementarity(Boolean pedagogicalTrainingComplementarity) {
        this.pedagogicalTrainingComplementarity = pedagogicalTrainingComplementarity;
    }

    public String getCodeCourse() {
        return codeCourse;
    }

    public void setCodeCourse(String codeCourse) {
        this.codeCourse = codeCourse;
    }

    public Date getStartYear() {
        return startYear;
    }

    public void setStartYear(Date startYear) {
        this.startYear = startYear;
    }

    public Date getEndYear() {
        return endYear;
    }

    public void setEndYear(Date endYear) {
        this.endYear = endYear;
    }

    public String getInstituteName() {
        return instituteName;
    }

    public void setInstituteName(String instituteName) {
        this.instituteName = instituteName;
    }

    public String getCodeUniversityINEP() {
        return codeUniversityINEP;
    }

    public void setCodeUniversityINEP(String codeUniversityINEP) {
        this.codeUniversityINEP = codeUniversityINEP;
    }
}
