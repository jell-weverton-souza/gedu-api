package com.manager.gedu.domain.task;

import com.manager.gedu.abstracts.AbstractDomain;
import com.manager.gedu.enums.TypeScoreRecord;

import java.util.List;

public class ScoreRecord extends AbstractDomain {
    private List<String> observations;
    private TypeScoreRecord typeScoreRecord;
    private Double score;

    public ScoreRecord() {}

    public List<String> getObservations() {
        return observations;
    }

    public ScoreRecord setObservations(List<String> observations) {
        this.observations = observations;
        return this;
    }

    public TypeScoreRecord getTypeScoreRecord() {
        return typeScoreRecord;
    }

    public ScoreRecord setTypeScoreRecord(TypeScoreRecord typeScoreRecord) {
        this.typeScoreRecord = typeScoreRecord;
        return this;
    }

    public Double getScore() {
        return score;
    }

    public ScoreRecord setScore(Double score) {
        this.score = score;
        return this;
    }
}
