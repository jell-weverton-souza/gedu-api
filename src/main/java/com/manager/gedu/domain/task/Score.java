package com.manager.gedu.domain.task;

import com.manager.gedu.abstracts.AbstractDomain;
import com.manager.gedu.domain.gradesubject.Subject;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(value = "score")
public class Score extends AbstractDomain {
    @DBRef
    private Subject subject;
    private List<ScoreRecord> scoreRecords;
    private Boolean result;

    public Score() {}

    public Subject getSubject() {
        return subject;
    }

    public Score setSubject(Subject subject) {
        this.subject = subject;
        return this;
    }

    public List<ScoreRecord> getScoreRecords() {
        return scoreRecords;
    }

    public Score setScoreRecords(List<ScoreRecord> scoreRecords) {
        this.scoreRecords = scoreRecords;
        return this;
    }

    public Boolean getResult() {
        return result;
    }

    public Score setResult(final Boolean result) {
        this.result = result;
        return this;
    }
}
