package com.manager.gedu.domain;

import com.manager.gedu.abstracts.AbstractDomain;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author Weverton Souza.
 * Created on 01/08/2019
 */
@Document
public class Company  extends AbstractDomain {
    private Integer businessSector;
    private String businessSectorDescription;
    private String taxIdentificationNumber;
    private String regulation;

    public Integer getBusinessSector() {
        return businessSector;
    }

    public void setBusinessSector(Integer businessSector) {
        this.businessSector = businessSector;
    }

    public String getBusinessSectorDescription() {
        return businessSectorDescription;
    }

    public void setBusinessSectorDescription(String businessSectorDescription) {
        this.businessSectorDescription = businessSectorDescription;
    }

    public String getTaxIdentificationNumber() {
        return taxIdentificationNumber;
    }

    public void setTaxIdentificationNumber(String taxIdentificationNumber) {
        this.taxIdentificationNumber = taxIdentificationNumber;
    }

    public String getRegulation() {
        return regulation;
    }

    public void setRegulation(String regulation) {
        this.regulation = regulation;
    }
}
