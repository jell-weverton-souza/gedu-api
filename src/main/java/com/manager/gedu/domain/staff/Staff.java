package com.manager.gedu.domain.staff;

import com.manager.gedu.abstracts.AbstractDomain;
import com.manager.gedu.domain.user.User;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author Weverton Souza.
 * Created on 24/08/19
 */
@Document(value = "staff")
public class Staff extends AbstractDomain {
    @DBRef
    private User userData;
    @DBRef
    private StaffType staffType;
    private String formation;
    private String specialty;
    private String category;

    public Staff() {}

    public String getFormation() {
        return formation;
    }

    public Staff setFormation(String formation) {
        this.formation = formation;
        return this;
    }

    public String getSpecialty() {
        return specialty;
    }

    public Staff setSpecialty(String specialty) {
        this.specialty = specialty;
        return this;
    }

    public String getCategory() {
        return category;
    }

    public Staff setCategory(String category) {
        this.category = category;
        return this;
    }

    public StaffType getStaffType() {
        return staffType;
    }

    public Staff setStaffType(StaffType staffType) {
        this.staffType = staffType;
        return this;
    }

    public User getUserData() {
        return userData;
    }

    public Staff setUserData(User userData) {
        this.userData = userData;
        return this;
    }
}
