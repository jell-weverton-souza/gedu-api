package com.manager.gedu.domain.staff;

import com.manager.gedu.abstracts.AbstractDomain;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author Weverton Souza.
 * Created on 24/08/19
 */
@Document(value = "staff_type")
public class StaffType extends AbstractDomain {
    private String name;
    private String abbreviation;
    private String description;

    public StaffType() {
    }

    public String getName() {
        return name;
    }

    public StaffType setName(String name) {
        this.name = name;
        return this;
    }

    public String getAbbreviation() {
        return this.abbreviation;
    }

    public StaffType setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public StaffType setDescription(String description) {
        this.description = description;
        return this;
    }
}
