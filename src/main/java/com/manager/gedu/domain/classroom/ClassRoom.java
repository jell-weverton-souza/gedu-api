package com.manager.gedu.domain.classroom;

import com.manager.gedu.abstracts.AbstractDomain;
import com.manager.gedu.domain.INEPData;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/**
 * @author Weverton Souza.
 * Created on 01/08/2019
 */
@Document
public class ClassRoom extends AbstractDomain {
    private INEPData inepData;
    private String classRoomName;
    private ClassRoomRegistration serviceType;
    private ClassRoomRegistration participatingInnovativeHighSchoolMoreEducation;
    private List<ClassRoomRegistration> additionalActivityType;
    private ClassRoomRegistration modality;
    private Integer teachingStage;
    private Integer codeCourseProfessionalEducation;
    private List<ClassRoomRegistration> subjects;
    private Boolean ClassroomWithoutUnitProfessional;

    public ClassRoom() {}

    public INEPData getInepData() {
        return inepData;
    }

    public void setInepData(INEPData inepData) {
        this.inepData = inepData;
    }

    public String getClassRoomName() {
        return classRoomName;
    }

    public void setClassRoomName(String classRoomName) {
        this.classRoomName = classRoomName;
    }

    public ClassRoomRegistration getServiceType() {
        return serviceType;
    }

    public void setServiceType(ClassRoomRegistration serviceType) {
        this.serviceType = serviceType;
    }

    public ClassRoomRegistration getParticipatingInnovativeHighSchoolMoreEducation() {
        return participatingInnovativeHighSchoolMoreEducation;
    }

    public void setParticipatingInnovativeHighSchoolMoreEducation(ClassRoomRegistration participatingInnovativeHighSchoolMoreEducation) {
        this.participatingInnovativeHighSchoolMoreEducation = participatingInnovativeHighSchoolMoreEducation;
    }

    public List<ClassRoomRegistration> getAdditionalActivityType() {
        return additionalActivityType;
    }

    public void setAdditionalActivityType(List<ClassRoomRegistration> additionalActivityType) {
        this.additionalActivityType = additionalActivityType;
    }

    public ClassRoomRegistration getModality() {
        return modality;
    }

    public void setModality(ClassRoomRegistration modality) {
        this.modality = modality;
    }

    public Integer getTeachingStage() {
        return teachingStage;
    }

    public void setTeachingStage(Integer teachingStage) {
        this.teachingStage = teachingStage;
    }

    public Integer getCodeCourseProfessionalEducation() {
        return codeCourseProfessionalEducation;
    }

    public void setCodeCourseProfessionalEducation(Integer codeCourseProfessionalEducation) {
        this.codeCourseProfessionalEducation = codeCourseProfessionalEducation;
    }

    public List<ClassRoomRegistration> getSubjects() {
        return subjects;
    }

    public void setSubjects(List<ClassRoomRegistration> subjects) {
        this.subjects = subjects;
    }

    public Boolean getClassroomWithoutUnitProfessional() {
        return ClassroomWithoutUnitProfessional;
    }

    public void setClassroomWithoutUnitProfessional(Boolean classroomWithoutUnitProfessional) {
        ClassroomWithoutUnitProfessional = classroomWithoutUnitProfessional;
    }
}
