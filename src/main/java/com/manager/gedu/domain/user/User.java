package com.manager.gedu.domain.user;

import com.manager.gedu.abstracts.AbstractDomain;
import com.manager.gedu.domain.address.Address;
import com.manager.gedu.enums.Roles;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/**
 * @author Weverton Souza.
 * Created on 28/06/19
 */
@Document("user")
public class User extends AbstractDomain {
    private String userCode;
    private String email;
    @DBRef
    private Person personalData;
    private List<FamilyMember> familyData;
    @DBRef
    private List<Address> addresses;
    private List<Roles> roles;

    public User() {}

    public String getUserCode() {
        return userCode;
    }

    public User setUserCode(String userCode) {
        this.userCode = userCode;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public User setEmail(String email) {
        this.email = email;
        return this;
    }

    public Person getPersonalData() {
        return personalData;
    }

    public User setPersonalData(Person personalData) {
        this.personalData = personalData;
        return this;
    }

    public List<Address> getAddresses() {
        return addresses;
    }

    public User setAddresses(List<Address> addresses) {
        this.addresses = addresses;
        return this;
    }

    public List<Roles> getRoles() {
        return roles;
    }

    public User setRoles(List<Roles> roles) {
        this.roles = roles;
        return this;
    }

    public List<FamilyMember> getFamilyData() {
        return familyData;
    }

    public User setFamilyData(List<FamilyMember> familyData) {
        this.familyData = familyData;
        return this;
    }
}
