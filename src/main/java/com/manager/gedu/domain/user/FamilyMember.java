package com.manager.gedu.domain.user;

import com.manager.gedu.abstracts.AbstractDomain;
import com.manager.gedu.enums.DegreeOfKinship;
import org.springframework.data.mongodb.core.mapping.DBRef;

public class FamilyMember extends AbstractDomain {
    @DBRef
    private Person memberData;
    private DegreeOfKinship degreeOfKinship;

    public FamilyMember() {}

    public Person getMemberData() {
        return memberData;
    }

    public FamilyMember setMemberData(Person memberData) {
        this.memberData = memberData;
        return this;
    }

    public DegreeOfKinship getDegreeOfKinship() {
        return degreeOfKinship;
    }

    public FamilyMember setDegreeOfKinship(DegreeOfKinship degreeOfKinship) {
        this.degreeOfKinship = degreeOfKinship;
        return this;
    }
}
