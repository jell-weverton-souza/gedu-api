package com.manager.gedu.domain.user;

import com.manager.gedu.abstracts.AbstractDomain;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author Weverton Souza.
 * Created on 24/08/19
 */
@Document(value = "person")
public class Person extends AbstractDomain {
    private String firstName;
    private String lastName;
    private String placeOfBirth;
    private String dateOfBirth;
    private String gender;
    private String cpf;
    private String rg;

    public Person() {}

    public String getFirstName() {
        return firstName;
    }

    public Person setFirstName(final String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public Person setLastName(final String lastName) {
        this.lastName = lastName;
        return this;
    }

    public String getPlaceOfBirth() {
        return placeOfBirth;
    }

    public Person setPlaceOfBirth(final String placeOfBirth) {
        this.placeOfBirth = placeOfBirth;
        return this;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public Person setDateOfBirth(final String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
        return this;
    }

    public String getGender() {
        return gender;
    }

    public Person setGender(final String gender) {
        this.gender = gender;
        return this;
    }

    public String getCpf() {
        return cpf;
    }

    public Person setCpf(final String cpf) {
        this.cpf = cpf;
        return this;
    }

    public String getRg() {
        return rg;
    }

    public Person setRg(final String rg) {
        this.rg = rg;
        return this;
    }
}
