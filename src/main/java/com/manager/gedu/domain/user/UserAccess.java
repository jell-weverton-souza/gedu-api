package com.manager.gedu.domain.user;

import com.manager.gedu.abstracts.AbstractDomain;
import com.manager.gedu.enums.Roles;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collection;
import java.util.Set;

/**
 * @author Weverton Souza.
 * Created on 28/06/19
 */
@Document(value = "user_access")
public class UserAccess extends AbstractDomain {
    private String userCode;
    private String email;
    private String password;
    private boolean credentialsNonExpired;
    private boolean accountNonLocked;
    private boolean accountNonExpired;
    private boolean enabled;
    private String typeAuthorization;
    private String token;
    private Set<Roles> roles;

    public UserAccess() {}

    public String getUserCode() {
        return userCode;
    }

    public UserAccess setUserCode(final String userCode) {
        this.userCode = userCode;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public UserAccess setEmail(final String email) {
        this.email = email;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public UserAccess setPassword(final String password) {
        this.password = password;
        return this;
    }

    public boolean isCredentialsNonExpired() {
        return credentialsNonExpired;
    }

    public UserAccess setCredentialsNonExpired(final boolean credentialsNonExpired) {
        this.credentialsNonExpired = credentialsNonExpired;
        return this;
    }

    public boolean isAccountNonLocked() {
        return accountNonLocked;
    }

    public UserAccess setAccountNonLocked(final boolean accountNonLocked) {
        this.accountNonLocked = accountNonLocked;
        return this;
    }

    public boolean isAccountNonExpired() {
        return accountNonExpired;
    }

    public UserAccess setAccountNonExpired(final boolean accountNonExpired) {
        this.accountNonExpired = accountNonExpired;
        return this;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public UserAccess setEnabled(final boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    public String getTypeAuthorization() {
        return typeAuthorization;
    }

    public UserAccess setTypeAuthorization(final String typeAuthorization) {
        this.typeAuthorization = typeAuthorization;
        return this;
    }

    public String getToken() {
        return token;
    }

    public UserAccess setToken(final String token) {
        this.token = token;
        return this;
    }

    public Set<Roles> getRoles() {
        return roles;
    }

    public UserAccess setRoles(final Set<Roles> roles) {
        this.roles = roles;
        return this;
    }
}
