package com.manager.gedu.domain.gradesubject;

import com.manager.gedu.abstracts.AbstractDomain;
import com.manager.gedu.enums.TypeGradeLevel;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(value = "grade_level")
public class GradeLevel extends AbstractDomain {
    private TypeGradeLevel level;
    private String name;
    private String abbreviation;
    private String description;
    private List<String> observations;

    public GradeLevel() {}

    public TypeGradeLevel getLevel() {
        return level;
    }

    public GradeLevel setLevel(TypeGradeLevel level) {
        this.level = level;
        return this;
    }

    public String getName() {
        return name;
    }

    public GradeLevel setName(String name) {
        this.name = name;
        return this;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public GradeLevel setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public GradeLevel setDescription(String description) {
        this.description = description;
        return this;
    }

    public List<String> getObservations() {
        return observations;
    }

    public GradeLevel setObservations(List<String> observations) {
        this.observations = observations;
        return this;
    }
}
