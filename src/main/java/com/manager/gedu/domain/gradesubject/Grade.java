package com.manager.gedu.domain.gradesubject;

import com.manager.gedu.abstracts.AbstractDomain;
import com.manager.gedu.enums.TypePeriod;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Year;
import java.util.Date;
import java.util.List;

@Document(value = "grade")
public class Grade extends AbstractDomain {
    private GradeLevel gradeLevel;
    private List<Subject> subjects;
    private List<Subject> diversifiedSubjects;
    private List<String> observations;
    private TypePeriod typePeriod;
    private Date dateStartPeriod;
    private Date dateEndPeriod;
    private Year year;

    public Grade() {}

    public GradeLevel getGradeLevel() {
        return gradeLevel;
    }

    public Grade setGradeLevel(final GradeLevel gradeLevel) {
        this.gradeLevel = gradeLevel;
        return this;
    }

    public List<Subject> getSubjects() {
        return subjects;
    }

    public Grade setSubjects(final List<Subject> subjects) {
        this.subjects = subjects;
        return this;
    }

    public List<Subject> getDiversifiedSubjects() {
        return diversifiedSubjects;
    }

    public Grade setDiversifiedSubjects(final List<Subject> diversifiedSubjects) {
        this.diversifiedSubjects = diversifiedSubjects;
        return this;
    }

    public List<String> getObservations() {
        return observations;
    }

    public Grade setObservations(final List<String> observations) {
        this.observations = observations;
        return this;
    }

    public TypePeriod getTypePeriod() {
        return typePeriod;
    }

    public Grade setTypePeriod(final TypePeriod typePeriod) {
        this.typePeriod = typePeriod;
        return this;
    }

    public Date getDateStartPeriod() {
        return dateStartPeriod;
    }

    public Grade setDateStartPeriod(final Date dateStartPeriod) {
        this.dateStartPeriod = dateStartPeriod;
        return this;
    }

    public Date getDateEndPeriod() {
        return dateEndPeriod;
    }

    public Grade setDateEndPeriod(final Date dateEndPeriod) {
        this.dateEndPeriod = dateEndPeriod;
        return this;
    }

    public Year getYear() {
        return year;
    }

    public Grade setYear(final Year year) {
        this.year = year;
        return this;
    }
}
