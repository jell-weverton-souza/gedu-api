package com.manager.gedu.domain.gradesubject;

import com.manager.gedu.abstracts.AbstractDomain;
import com.manager.gedu.domain.staff.Staff;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(value = "subject")
public class Subject extends AbstractDomain {
    private String name;
    private String abbreviation;
    private String description;
    private List<String> observations;
    private List<Staff> teachers;

    public Subject() {}

    public String getName() {
        return name;
    }

    public Subject setName(String name) {
        this.name = name;
        return this;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public Subject setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Subject setDescription(String description) {
        this.description = description;
        return this;
    }

    public List<String> getObservations() {
        return observations;
    }

    public Subject setObservations(List<String> observations) {
        this.observations = observations;
        return this;
    }

    public List<Staff> getTeachers() {
        return teachers;
    }

    public Subject setTeachers(List<Staff> teachers) {
        this.teachers = teachers;
        return this;
    }
}
