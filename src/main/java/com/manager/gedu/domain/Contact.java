package com.manager.gedu.domain;

import com.manager.gedu.abstracts.AbstractDomain;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(value = "contact")
public class Contact extends AbstractDomain {
    private String homePhone;
    private String mobilephone;
    private String workPhone;
    private String personalEmail;
    private String workEmail;
    private String otherPhone;
    private String facebook;
    private String twitter;
    private String linkedin;

    public Contact() {}

    public String getHomePhone() {
        return homePhone;
    }

    public Contact setHomePhone(String homePhone) {
        this.homePhone = homePhone;
        return this;
    }

    public String getMobilephone() {
        return mobilephone;
    }

    public Contact setMobilephone(String mobilephone) {
        this.mobilephone = mobilephone;
        return this;
    }

    public String getWorkPhone() {
        return workPhone;
    }

    public Contact setWorkPhone(String workPhone) {
        this.workPhone = workPhone;
        return this;
    }

    public String getPersonalEmail() {
        return personalEmail;
    }

    public Contact setPersonalEmail(String personalEmail) {
        this.personalEmail = personalEmail;
        return this;
    }

    public String getWorkEmail() {
        return workEmail;
    }

    public Contact setWorkEmail(String workEmail) {
        this.workEmail = workEmail;
        return this;
    }

    public String getOtherPhone() {
        return otherPhone;
    }

    public Contact setOtherPhone(String otherPhone) {
        this.otherPhone = otherPhone;
        return this;
    }

    public String getFacebook() {
        return facebook;
    }

    public Contact setFacebook(String facebook) {
        this.facebook = facebook;
        return this;
    }

    public String getTwitter() {
        return twitter;
    }

    public Contact setTwitter(String twitter) {
        this.twitter = twitter;
        return this;
    }

    public String getLinkedin() {
        return linkedin;
    }

    public Contact setLinkedin(String linkedin) {
        this.linkedin = linkedin;
        return this;
    }
}
