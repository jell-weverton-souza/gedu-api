package com.manager.gedu.datatransferobject.student;

import com.manager.gedu.abstracts.AbstractDataTransferObject;
import com.manager.gedu.datatransferobject.gradesubject.GradeDTO;
import com.manager.gedu.datatransferobject.task.ScoreDTO;
import com.manager.gedu.datatransferobject.user.UserDTO;
import org.springframework.data.mongodb.core.mapping.DBRef;

/**
 * @author Weverton Souza.
 * Created on 01/08/2019
 */
public class StudentDTO extends AbstractDataTransferObject {
    @DBRef
    private UserDTO userData;
    @DBRef
    private GradeDTO currentGrade;
    @DBRef
    private ScoreDTO score;

    public StudentDTO() {}

    public UserDTO getUserData() {
        return userData;
    }

    public StudentDTO setUserData(final UserDTO userData) {
        this.userData = userData;
        return this;
    }

    public GradeDTO getCurrentGrade() {
        return currentGrade;
    }

    public StudentDTO setCurrentGrade(final GradeDTO currentGrade) {
        this.currentGrade = currentGrade;
        return this;
    }

    public ScoreDTO getScore() {
        return score;
    }

    public StudentDTO setScore(final ScoreDTO score) {
        this.score = score;
        return this;
    }
}
