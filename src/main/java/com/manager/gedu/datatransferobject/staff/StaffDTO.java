package com.manager.gedu.datatransferobject.staff;

import com.manager.gedu.abstracts.AbstractDataTransferObject;
import com.manager.gedu.abstracts.AbstractDomain;
import com.manager.gedu.datatransferobject.user.UserDTO;
import com.manager.gedu.domain.user.User;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author Weverton Souza.
 * Created on 24/08/19
 */
@Document(value = "staff")
public class StaffDTO extends AbstractDataTransferObject {
    @DBRef
    private UserDTO userData;
    @DBRef
    private StaffTypeDTO staffType;
    private String formation;
    private String specialty;
    private String category;

    public StaffDTO() {}

    public UserDTO getUserData() {
        return userData;
    }

    public StaffDTO setUserData(final UserDTO userData) {
        this.userData = userData;
        return this;
    }

    public StaffTypeDTO getStaffType() {
        return staffType;
    }

    public StaffDTO setStaffType(final StaffTypeDTO staffType) {
        this.staffType = staffType;
        return this;
    }

    public String getFormation() {
        return formation;
    }

    public StaffDTO setFormation(final String formation) {
        this.formation = formation;
        return this;
    }

    public String getSpecialty() {
        return specialty;
    }

    public StaffDTO setSpecialty(final String specialty) {
        this.specialty = specialty;
        return this;
    }

    public String getCategory() {
        return category;
    }

    public StaffDTO setCategory(final String category) {
        this.category = category;
        return this;
    }
}
