package com.manager.gedu.datatransferobject.staff;

import com.manager.gedu.abstracts.AbstractDataTransferObject;
import com.manager.gedu.abstracts.AbstractDomain;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author Weverton Souza.
 * Created on 24/08/19
 */
@Document(value = "staff_type")
public class StaffTypeDTO extends AbstractDataTransferObject {
    private String name;
    private String abbreviation;
    private String description;

    public StaffTypeDTO() {
    }

    public String getName() {
        return name;
    }

    public StaffTypeDTO setName(String name) {
        this.name = name;
        return this;
    }

    public String getAbbreviation() {
        return this.abbreviation;
    }

    public StaffTypeDTO setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public StaffTypeDTO setDescription(String description) {
        this.description = description;
        return this;
    }
}
