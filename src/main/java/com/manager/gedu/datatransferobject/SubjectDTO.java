package com.manager.gedu.datatransferobject;

import com.manager.gedu.abstracts.AbstractDataTransferObject;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import static com.manager.gedu.api.DataTranferObjectConstants.*;

/**
 * @author Weverton Souza.
 * Created on 24/07/19
 */
@ApiModel(value= UNIT_SUBJECT, description= UNIT_SUBJECT_CLASS_DESCRIPTION)
public class SubjectDTO extends AbstractDataTransferObject {
    @ApiModelProperty(notes=UNIT_SUBJECT_TYPE)
    private String subjectType;
    @ApiModelProperty(notes=UNIT_SUBJECT_NAME)
    private String subjectName;
    @ApiModelProperty(notes=UNIT_SUBJECT_FIELD_DESCRIPTION)
    private String subjectDescription;

    public SubjectDTO() {}

    public String getSubjectType() {
        return subjectType;
    }

    public SubjectDTO setSubjectType(String subjectType) {
        this.subjectType = subjectType;
        return this;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public SubjectDTO setSubjectName(String subjectName) {
        this.subjectName = subjectName;
        return this;
    }

    public String getSubjectDescription() {
        return subjectDescription;
    }

    public SubjectDTO setSubjectDescription(String subjectDescription) {
        this.subjectDescription = subjectDescription;
        return this;
    }
}
