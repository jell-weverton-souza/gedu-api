package com.manager.gedu.datatransferobject.task;

import com.manager.gedu.abstracts.AbstractDataTransferObject;
import com.manager.gedu.abstracts.AbstractDomain;
import com.manager.gedu.domain.gradesubject.Subject;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(value = "score")
public class ScoreDTO extends AbstractDataTransferObject {
    @DBRef
    private Subject subject;
    private List<ScoreRecordDTO> scoreRecordDTOS;

    public ScoreDTO() {}

    public Subject getSubject() {
        return subject;
    }

    public ScoreDTO setSubject(Subject subject) {
        this.subject = subject;
        return this;
    }

    public List<ScoreRecordDTO> getScoreRecordDTOS() {
        return scoreRecordDTOS;
    }

    public ScoreDTO setScoreRecordDTOS(List<ScoreRecordDTO> scoreRecordDTOS) {
        this.scoreRecordDTOS = scoreRecordDTOS;
        return this;
    }
}
