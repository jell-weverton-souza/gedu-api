package com.manager.gedu.datatransferobject.task;

import com.manager.gedu.abstracts.AbstractDomain;
import com.manager.gedu.enums.TypeScoreRecord;

import java.util.List;

public class ScoreRecordDTO extends AbstractDomain {
    private List<String> observations;
    private TypeScoreRecord typeScoreRecord;
    private Double score;

    public ScoreRecordDTO() {}

    public List<String> getObservations() {
        return observations;
    }

    public ScoreRecordDTO setObservations(List<String> observations) {
        this.observations = observations;
        return this;
    }

    public TypeScoreRecord getTypeScoreRecord() {
        return typeScoreRecord;
    }

    public ScoreRecordDTO setTypeScoreRecord(TypeScoreRecord typeScoreRecord) {
        this.typeScoreRecord = typeScoreRecord;
        return this;
    }

    public Double getScore() {
        return score;
    }

    public ScoreRecordDTO setScore(Double score) {
        this.score = score;
        return this;
    }
}
