package com.manager.gedu.datatransferobject.gradesubject;

import com.manager.gedu.abstracts.AbstractDataTransferObject;
import com.manager.gedu.abstracts.AbstractDomain;
import com.manager.gedu.enums.TypePeriod;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Year;
import java.util.Date;
import java.util.List;

public class GradeDTO extends AbstractDataTransferObject {
    private GradeLevelDTO gradeLevel;
    private List<SubjectDTO> subjects;
    private List<String> observations;
    private TypePeriod typePeriod;
    private Date dateStartPeriod;
    private Date dateEndPeriod;
    private Year year;

    public GradeDTO() {}

    public GradeLevelDTO getGradeLevel() {
        return gradeLevel;
    }

    public GradeDTO setGradeLevel(final GradeLevelDTO gradeLevel) {
        this.gradeLevel = gradeLevel;
        return this;
    }

    public List<SubjectDTO> getSubjects() {
        return subjects;
    }

    public GradeDTO setSubjects(final List<SubjectDTO> subjects) {
        this.subjects = subjects;
        return this;
    }

    public List<String> getObservations() {
        return observations;
    }

    public GradeDTO setObservations(final List<String> observations) {
        this.observations = observations;
        return this;
    }

    public TypePeriod getTypePeriod() {
        return typePeriod;
    }

    public GradeDTO setTypePeriod(final TypePeriod typePeriod) {
        this.typePeriod = typePeriod;
        return this;
    }

    public Date getDateStartPeriod() {
        return dateStartPeriod;
    }

    public GradeDTO setDateStartPeriod(final Date dateStartPeriod) {
        this.dateStartPeriod = dateStartPeriod;
        return this;
    }

    public Date getDateEndPeriod() {
        return dateEndPeriod;
    }

    public GradeDTO setDateEndPeriod(final Date dateEndPeriod) {
        this.dateEndPeriod = dateEndPeriod;
        return this;
    }

    public Year getYear() {
        return year;
    }

    public GradeDTO setYear(final Year year) {
        this.year = year;
        return this;
    }
}
