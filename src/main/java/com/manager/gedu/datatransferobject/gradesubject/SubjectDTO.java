package com.manager.gedu.datatransferobject.gradesubject;

import com.manager.gedu.abstracts.AbstractDomain;
import com.manager.gedu.domain.staff.Staff;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

public class SubjectDTO extends AbstractDomain {
    private String name;
    private String abbreviation;
    private String description;
    private List<String> observations;
    private List<Staff> teachers;

    public SubjectDTO() {}

    public String getName() {
        return name;
    }

    public SubjectDTO setName(final String name) {
        this.name = name;
        return this;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public SubjectDTO setAbbreviation(final String abbreviation) {
        this.abbreviation = abbreviation;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public SubjectDTO setDescription(final String description) {
        this.description = description;
        return this;
    }

    public List<String> getObservations() {
        return observations;
    }

    public SubjectDTO setObservations(final List<String> observations) {
        this.observations = observations;
        return this;
    }

    public List<Staff> getTeachers() {
        return teachers;
    }

    public SubjectDTO setTeachers(final List<Staff> teachers) {
        this.teachers = teachers;
        return this;
    }
}
