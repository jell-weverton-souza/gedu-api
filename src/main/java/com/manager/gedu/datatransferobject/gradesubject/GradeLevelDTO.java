package com.manager.gedu.datatransferobject.gradesubject;

import com.manager.gedu.abstracts.AbstractDomain;
import com.manager.gedu.enums.TypeGradeLevel;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

public class GradeLevelDTO extends AbstractDomain {
    private TypeGradeLevel level;
    private String name;
    private String abbreviation;
    private String description;
    private List<String> observations;

    public GradeLevelDTO() {}

    public TypeGradeLevel getLevel() {
        return level;
    }

    public GradeLevelDTO setLevel(final TypeGradeLevel level) {
        this.level = level;
        return this;
    }

    public String getName() {
        return name;
    }

    public GradeLevelDTO setName(final String name) {
        this.name = name;
        return this;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public GradeLevelDTO setAbbreviation(final String abbreviation) {
        this.abbreviation = abbreviation;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public GradeLevelDTO setDescription(final String description) {
        this.description = description;
        return this;
    }

    public List<String> getObservations() {
        return observations;
    }

    public GradeLevelDTO setObservations(final List<String> observations) {
        this.observations = observations;
        return this;
    }
}
