package com.manager.gedu.datatransferobject.unit;

import com.manager.gedu.abstracts.AbstractDataTransferObject;
import com.manager.gedu.interfaces.IDataTransferObject;

import java.util.Date;
import java.util.List;

/**
 * @author Weverton Souza.
 * Created on 01/08/2019
 */
public class InfrastructureRegistrationDTO extends AbstractDataTransferObject {
    private String registerType;
    private String registerName;
    private Integer registerCode;
    private String registerDescription;
    private List<String> roles;

    public InfrastructureRegistrationDTO() {}

    @Override
    public String getId() {
        return super.getId();
    }

    @Override
    public IDataTransferObject setId(String id) {
        return super.setId(id);
    }

    public String getRegisterType() {
        return registerType;
    }

    public void setRegisterType(String registerType) {
        this.registerType = registerType;
    }

    public String getRegisterName() {
        return registerName;
    }

    public void setRegisterName(String registerName) {
        this.registerName = registerName;
    }

    public Integer getRegisterCode() {
        return registerCode;
    }

    public void setRegisterCode(Integer registerCode) {
        this.registerCode = registerCode;
    }

    public String getRegisterDescription() {
        return registerDescription;
    }

    public void setRegisterDescription(String registerDescription) {
        this.registerDescription = registerDescription;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }
}
