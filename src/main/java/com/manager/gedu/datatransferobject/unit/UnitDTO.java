package com.manager.gedu.datatransferobject.unit;

import com.manager.gedu.abstracts.AbstractDataTransferObject;

/**
 * @author Weverton Souza.
 * Created on 01/08/2019
 */
public class UnitDTO extends AbstractDataTransferObject {
    private Integer year;
    private Integer unitCod;
    private String unitName;
    private String regionalAgencyCod;
    private Long operationSituation;
    private String academicYearStart;
    private String academicYearEnd;
    private Long regionCod;
    private Long mesoregionCod;
    private Long microregionCod;
    private Long uf;
    private Long countyCod;
    private Long districtCod;
    private Long administrativeDependence;
    private Long location;
    private Long privateUnitCategory;
    private Long associatedWithTheGovernment;
    private Long governmentDependece;
    private Long privateUnitMaintainer;
    private Long privateUnitMaintainerONG;
    private Long privateUnitMaintainerSind;
    private Long privateUnitMaintainerSSystem;
    private Long privateUnitMaintainerFilantropic;
    private Long unitHeadquarters;
    private Long IESCode;
    private Long governmentRegulation;
    private Long opUnitBuilding;
    private Long unitOccupancyType;
    private Long opCompanyRooms;
    private Long opSocioeducationalCareUnit;
    private Long opPrisonUnit;
    private Long opSocioeducationalCareUnitOrPrisonUnit;
    private Long opChurchOrTemple;
    private Long opTeachersHouse;
    private Long opShed;
    private Long shedOccupancyType;
    private Long opRoomsInAnotherUnit;
    private Long opOthers;
    private Long sharedBuilding;
    private Long filteredWater;
    private Long waterPublicNetwork;
    private Long waterArtesianWell;
    private Long waterCacimba;
    private Long waterRiver;
    private Long waterNoneExistent;
    private Long eletricPowerPublicNetwork;
    private Long eletricPowerGenerator;
    private Long eletricPowerOthers;
    private Long eletricPowerNoneExistent;
    private Long sanitarySewagePublicNetwork;
    private Long sanitarySewageSepticTank;
    private Long sanitarySewageNoneExistent;
    private Long wastePeriodicCollect;
    private Long wasteBorn;
    private Long wasteThrowAway;
    private Long wasteRecycle;
    private Long wasteBury;
    private Long wasteOthers;
    private Long depBoardRoom;
    private Long depTeachersRoom;
    private Long depComputerLab;
    private Long depSpecialAttendance;
    private Long depSciencelab;
    private Long depSportCourtCovered;
    private Long depSportCourtNotCovered;
    private Long depSportCourt;
    private Long depKitchen;
    private Long depLibrary;
    private Long depReadingRoom;
    private Long depLibraryReadingRoom;
    private Long depPlayground;
    private Long depNurcery;
    private Long depBathroomOutsideBuilding;
    private Long depBathroomInsideBuilding;
    private Long depBathroomEI;
    private Long depBathroomPNE;
    private Long depPNE;
    private Long depSecretary;
    private Long depBathroomWithShower;
    private Long depRefectory;
    private Long depPantry;
    private Long depWarehouse;
    private Long depAuditorium;
    private Long depCourtyardCovered;
    private Long depCourtyardNotCovered;
    private Long depStudentAccommodation;
    private Long depTeacherAccommodation;
    private Long depGreenArea;
    private Long depLaundry;
    private Long depNoneOthers;
    private Long exNuExistingRooms;
    private Long exNuUsedRooms;
    private Long equipTv;
    private Long equipVideotape;
    private Long equipDVD;
    private Long equipParabolic;
    private Long equipCopyMachine;
    private Long equipOverheadProjector;
    private Long equipPrinter;
    private Long equipMultifunctionPrinter;
    private Long equipStereo;
    private Long equipDatashow;
    private Long equipFax;
    private Long equipPhoto;
    private Long equipComputer;
    private Long equipNuTv;
    private Long equipNuVideotape;
    private Long equipNuDVD;
    private Long equipNuParabolic;
    private Long equipNuCopyMachine;
    private Long equipNuOverheadProjector;
    private Long equipNuPrinter;
    private Long equipNuMultifunctionPrinter;
    private Long equipNuStereo;
    private Long equipNuDatashow;
    private Long equipNuFax;
    private Long equipNuPhoto;
    private Long equipNuComputer;
    private Long equipComputerAdm;
    private Long equipComputerStudent;
    private Long internet;
    private Long broadbandInternet;
    private Long employees;
    private Long food;
    private Long specializedEducationalService;
    private Long complementaryActivity;
    private Long elementaryUnitOrganizedInCycles;
    private Long differentiatedUnitriLocation;
    private Long mdeQuilombolas;
    private Long mdeIndigenous;
    private Long mdeNotUse;
    private Long indigenousEducation;
    private Long indigenousLanguageName;
    private Long indigenousLanguageCod;
    private Long brazilLiterate;
    private Long opensOnWeekends;
    private Long alternateFormation;
    private Long didacticPedagogicalPresential;
    private Long didacticPedagogicalSemiPresential;
    private Long didacticPedagogicalEAD;
    private Long exclusiveSpecial;
    private Long edRegular;
    private Long edEJA;
    private Long edProfessional;
    private Long coNursery;
    private Long coPre;
    private Long coFundAI;
    private Long coFundAF;
    private Long coMedioMP;
    private Long coMedioIN;
    private Long coMedioNo;
    private Long exSpNursery;
    private Long exSpPre;
    private Long exSpFundAI;
    private Long exSpFundAF;
    private Long exSpMedioMP;
    private Long exSpMedioIN;
    private Long exSpMedioNo;
    private Long coEJAFund;
    private Long coEJAMedio;
    private Long coEJAProf;
    private Long exSpEJAFund;
    private Long exSpEJAMedio;
    private Long exSpEJAProf;
    private Long coProf;
    private Long exSpProf;

    public UnitDTO() {}

    public Integer getYear() {
        return year;
    }

    public UnitDTO setYear(Integer year) {
        this.year = year;
        return this;
    }

    public Integer getUnitCod() {
        return unitCod;
    }

    public UnitDTO setUnitCod(Integer unitCod) {
        this.unitCod = unitCod;
        return this;
    }

    public String getUnitName() {
        return unitName;
    }

    public UnitDTO setUnitName(String unitName) {
        this.unitName = unitName;
        return this;
    }

    public String getRegionalAgencyCod() {
        return regionalAgencyCod;
    }

    public UnitDTO setRegionalAgencyCod(String regionalAgencyCod) {
        this.regionalAgencyCod = regionalAgencyCod;
        return this;
    }

    public Long getOperationSituation() {
        return operationSituation;
    }

    public UnitDTO setOperationSituation(Long operationSituation) {
        this.operationSituation = operationSituation;
        return this;
    }

    public String getAcademicYearStart() {
        return academicYearStart;
    }

    public UnitDTO setAcademicYearStart(String academicYearStart) {
        this.academicYearStart = academicYearStart;
        return this;
    }

    public String getAcademicYearEnd() {
        return academicYearEnd;
    }

    public UnitDTO setAcademicYearEnd(String academicYearEnd) {
        this.academicYearEnd = academicYearEnd;
        return this;
    }

    public Long getRegionCod() {
        return regionCod;
    }

    public UnitDTO setRegionCod(Long regionCod) {
        this.regionCod = regionCod;
        return this;
    }

    public Long getMesoregionCod() {
        return mesoregionCod;
    }

    public UnitDTO setMesoregionCod(Long mesoregionCod) {
        this.mesoregionCod = mesoregionCod;
        return this;
    }

    public Long getMicroregionCod() {
        return microregionCod;
    }

    public UnitDTO setMicroregionCod(Long microregionCod) {
        this.microregionCod = microregionCod;
        return this;
    }

    public Long getUf() {
        return uf;
    }

    public UnitDTO setUf(Long uf) {
        this.uf = uf;
        return this;
    }

    public Long getCountyCod() {
        return countyCod;
    }

    public UnitDTO setCountyCod(Long countyCod) {
        this.countyCod = countyCod;
        return this;
    }

    public Long getDistrictCod() {
        return districtCod;
    }

    public UnitDTO setDistrictCod(Long districtCod) {
        this.districtCod = districtCod;
        return this;
    }

    public Long getAdministrativeDependence() {
        return administrativeDependence;
    }

    public UnitDTO setAdministrativeDependence(Long administrativeDependence) {
        this.administrativeDependence = administrativeDependence;
        return this;
    }

    public Long getLocation() {
        return location;
    }

    public UnitDTO setLocation(Long location) {
        this.location = location;
        return this;
    }

    public Long getPrivateUnitCategory() {
        return privateUnitCategory;
    }

    public UnitDTO setPrivateUnitCategory(Long privateUnitCategory) {
        this.privateUnitCategory = privateUnitCategory;
        return this;
    }

    public Long getAssociatedWithTheGovernment() {
        return associatedWithTheGovernment;
    }

    public UnitDTO setAssociatedWithTheGovernment(Long associatedWithTheGovernment) {
        this.associatedWithTheGovernment = associatedWithTheGovernment;
        return this;
    }

    public Long getGovernmentDependece() {
        return governmentDependece;
    }

    public UnitDTO setGovernmentDependece(Long governmentDependece) {
        this.governmentDependece = governmentDependece;
        return this;
    }

    public Long getPrivateUnitMaintainer() {
        return privateUnitMaintainer;
    }

    public UnitDTO setPrivateUnitMaintainer(Long privateUnitMaintainer) {
        this.privateUnitMaintainer = privateUnitMaintainer;
        return this;
    }

    public Long getPrivateUnitMaintainerONG() {
        return privateUnitMaintainerONG;
    }

    public UnitDTO setPrivateUnitMaintainerONG(Long privateUnitMaintainerONG) {
        this.privateUnitMaintainerONG = privateUnitMaintainerONG;
        return this;
    }

    public Long getPrivateUnitMaintainerSind() {
        return privateUnitMaintainerSind;
    }

    public UnitDTO setPrivateUnitMaintainerSind(Long privateUnitMaintainerSind) {
        this.privateUnitMaintainerSind = privateUnitMaintainerSind;
        return this;
    }

    public Long getPrivateUnitMaintainerSSystem() {
        return privateUnitMaintainerSSystem;
    }

    public UnitDTO setPrivateUnitMaintainerSSystem(Long privateUnitMaintainerSSystem) {
        this.privateUnitMaintainerSSystem = privateUnitMaintainerSSystem;
        return this;
    }

    public Long getPrivateUnitMaintainerFilantropic() {
        return privateUnitMaintainerFilantropic;
    }

    public UnitDTO setPrivateUnitMaintainerFilantropic(Long privateUnitMaintainerFilantropic) {
        this.privateUnitMaintainerFilantropic = privateUnitMaintainerFilantropic;
        return this;
    }

    public Long getUnitHeadquarters() {
        return unitHeadquarters;
    }

    public UnitDTO setUnitHeadquarters(Long unitHeadquarters) {
        this.unitHeadquarters = unitHeadquarters;
        return this;
    }

    public Long getIESCode() {
        return IESCode;
    }

    public UnitDTO setIESCode(Long IESCode) {
        this.IESCode = IESCode;
        return this;
    }

    public Long getGovernmentRegulation() {
        return governmentRegulation;
    }

    public UnitDTO setGovernmentRegulation(Long governmentRegulation) {
        this.governmentRegulation = governmentRegulation;
        return this;
    }

    public Long getOpUnitBuilding() {
        return opUnitBuilding;
    }

    public UnitDTO setOpUnitBuilding(Long opUnitBuilding) {
        this.opUnitBuilding = opUnitBuilding;
        return this;
    }

    public Long getUnitOccupancyType() {
        return unitOccupancyType;
    }

    public UnitDTO setUnitOccupancyType(Long unitOccupancyType) {
        this.unitOccupancyType = unitOccupancyType;
        return this;
    }

    public Long getOpCompanyRooms() {
        return opCompanyRooms;
    }

    public UnitDTO setOpCompanyRooms(Long opCompanyRooms) {
        this.opCompanyRooms = opCompanyRooms;
        return this;
    }

    public Long getOpSocioeducationalCareUnit() {
        return opSocioeducationalCareUnit;
    }

    public UnitDTO setOpSocioeducationalCareUnit(Long opSocioeducationalCareUnit) {
        this.opSocioeducationalCareUnit = opSocioeducationalCareUnit;
        return this;
    }

    public Long getOpPrisonUnit() {
        return opPrisonUnit;
    }

    public UnitDTO setOpPrisonUnit(Long opPrisonUnit) {
        this.opPrisonUnit = opPrisonUnit;
        return this;
    }

    public Long getOpSocioeducationalCareUnitOrPrisonUnit() {
        return opSocioeducationalCareUnitOrPrisonUnit;
    }

    public UnitDTO setOpSocioeducationalCareUnitOrPrisonUnit(Long opSocioeducationalCareUnitOrPrisonUnit) {
        this.opSocioeducationalCareUnitOrPrisonUnit = opSocioeducationalCareUnitOrPrisonUnit;
        return this;
    }

    public Long getOpChurchOrTemple() {
        return opChurchOrTemple;
    }

    public UnitDTO setOpChurchOrTemple(Long opChurchOrTemple) {
        this.opChurchOrTemple = opChurchOrTemple;
        return this;
    }

    public Long getOpTeachersHouse() {
        return opTeachersHouse;
    }

    public UnitDTO setOpTeachersHouse(Long opTeachersHouse) {
        this.opTeachersHouse = opTeachersHouse;
        return this;
    }

    public Long getOpShed() {
        return opShed;
    }

    public UnitDTO setOpShed(Long opShed) {
        this.opShed = opShed;
        return this;
    }

    public Long getShedOccupancyType() {
        return shedOccupancyType;
    }

    public UnitDTO setShedOccupancyType(Long shedOccupancyType) {
        this.shedOccupancyType = shedOccupancyType;
        return this;
    }

    public Long getOpRoomsInAnotherUnit() {
        return opRoomsInAnotherUnit;
    }

    public UnitDTO setOpRoomsInAnotherUnit(Long opRoomsInAnotherUnit) {
        this.opRoomsInAnotherUnit = opRoomsInAnotherUnit;
        return this;
    }

    public Long getOpOthers() {
        return opOthers;
    }

    public UnitDTO setOpOthers(Long opOthers) {
        this.opOthers = opOthers;
        return this;
    }

    public Long getSharedBuilding() {
        return sharedBuilding;
    }

    public UnitDTO setSharedBuilding(Long sharedBuilding) {
        this.sharedBuilding = sharedBuilding;
        return this;
    }

    public Long getFilteredWater() {
        return filteredWater;
    }

    public UnitDTO setFilteredWater(Long filteredWater) {
        this.filteredWater = filteredWater;
        return this;
    }

    public Long getWaterPublicNetwork() {
        return waterPublicNetwork;
    }

    public UnitDTO setWaterPublicNetwork(Long waterPublicNetwork) {
        this.waterPublicNetwork = waterPublicNetwork;
        return this;
    }

    public Long getWaterArtesianWell() {
        return waterArtesianWell;
    }

    public UnitDTO setWaterArtesianWell(Long waterArtesianWell) {
        this.waterArtesianWell = waterArtesianWell;
        return this;
    }

    public Long getWaterCacimba() {
        return waterCacimba;
    }

    public UnitDTO setWaterCacimba(Long waterCacimba) {
        this.waterCacimba = waterCacimba;
        return this;
    }

    public Long getWaterRiver() {
        return waterRiver;
    }

    public UnitDTO setWaterRiver(Long waterRiver) {
        this.waterRiver = waterRiver;
        return this;
    }

    public Long getWaterNoneExistent() {
        return waterNoneExistent;
    }

    public UnitDTO setWaterNoneExistent(Long waterNoneExistent) {
        this.waterNoneExistent = waterNoneExistent;
        return this;
    }

    public Long getEletricPowerPublicNetwork() {
        return eletricPowerPublicNetwork;
    }

    public UnitDTO setEletricPowerPublicNetwork(Long eletricPowerPublicNetwork) {
        this.eletricPowerPublicNetwork = eletricPowerPublicNetwork;
        return this;
    }

    public Long getEletricPowerGenerator() {
        return eletricPowerGenerator;
    }

    public UnitDTO setEletricPowerGenerator(Long eletricPowerGenerator) {
        this.eletricPowerGenerator = eletricPowerGenerator;
        return this;
    }

    public Long getEletricPowerOthers() {
        return eletricPowerOthers;
    }

    public UnitDTO setEletricPowerOthers(Long eletricPowerOthers) {
        this.eletricPowerOthers = eletricPowerOthers;
        return this;
    }

    public Long getEletricPowerNoneExistent() {
        return eletricPowerNoneExistent;
    }

    public UnitDTO setEletricPowerNoneExistent(Long eletricPowerNoneExistent) {
        this.eletricPowerNoneExistent = eletricPowerNoneExistent;
        return this;
    }

    public Long getSanitarySewagePublicNetwork() {
        return sanitarySewagePublicNetwork;
    }

    public UnitDTO setSanitarySewagePublicNetwork(Long sanitarySewagePublicNetwork) {
        this.sanitarySewagePublicNetwork = sanitarySewagePublicNetwork;
        return this;
    }

    public Long getSanitarySewageSepticTank() {
        return sanitarySewageSepticTank;
    }

    public UnitDTO setSanitarySewageSepticTank(Long sanitarySewageSepticTank) {
        this.sanitarySewageSepticTank = sanitarySewageSepticTank;
        return this;
    }

    public Long getSanitarySewageNoneExistent() {
        return sanitarySewageNoneExistent;
    }

    public UnitDTO setSanitarySewageNoneExistent(Long sanitarySewageNoneExistent) {
        this.sanitarySewageNoneExistent = sanitarySewageNoneExistent;
        return this;
    }

    public Long getWastePeriodicCollect() {
        return wastePeriodicCollect;
    }

    public UnitDTO setWastePeriodicCollect(Long wastePeriodicCollect) {
        this.wastePeriodicCollect = wastePeriodicCollect;
        return this;
    }

    public Long getWasteBorn() {
        return wasteBorn;
    }

    public UnitDTO setWasteBorn(Long wasteBorn) {
        this.wasteBorn = wasteBorn;
        return this;
    }

    public Long getWasteThrowAway() {
        return wasteThrowAway;
    }

    public UnitDTO setWasteThrowAway(Long wasteThrowAway) {
        this.wasteThrowAway = wasteThrowAway;
        return this;
    }

    public Long getWasteRecycle() {
        return wasteRecycle;
    }

    public UnitDTO setWasteRecycle(Long wasteRecycle) {
        this.wasteRecycle = wasteRecycle;
        return this;
    }

    public Long getWasteBury() {
        return wasteBury;
    }

    public UnitDTO setWasteBury(Long wasteBury) {
        this.wasteBury = wasteBury;
        return this;
    }

    public Long getWasteOthers() {
        return wasteOthers;
    }

    public UnitDTO setWasteOthers(Long wasteOthers) {
        this.wasteOthers = wasteOthers;
        return this;
    }

    public Long getDepBoardRoom() {
        return depBoardRoom;
    }

    public UnitDTO setDepBoardRoom(Long depBoardRoom) {
        this.depBoardRoom = depBoardRoom;
        return this;
    }

    public Long getDepTeachersRoom() {
        return depTeachersRoom;
    }

    public UnitDTO setDepTeachersRoom(Long depTeachersRoom) {
        this.depTeachersRoom = depTeachersRoom;
        return this;
    }

    public Long getDepComputerLab() {
        return depComputerLab;
    }

    public UnitDTO setDepComputerLab(Long depComputerLab) {
        this.depComputerLab = depComputerLab;
        return this;
    }

    public Long getDepSpecialAttendance() {
        return depSpecialAttendance;
    }

    public UnitDTO setDepSpecialAttendance(Long depSpecialAttendance) {
        this.depSpecialAttendance = depSpecialAttendance;
        return this;
    }

    public Long getDepSciencelab() {
        return depSciencelab;
    }

    public UnitDTO setDepSciencelab(Long depSciencelab) {
        this.depSciencelab = depSciencelab;
        return this;
    }

    public Long getDepSportCourtCovered() {
        return depSportCourtCovered;
    }

    public UnitDTO setDepSportCourtCovered(Long depSportCourtCovered) {
        this.depSportCourtCovered = depSportCourtCovered;
        return this;
    }

    public Long getDepSportCourtNotCovered() {
        return depSportCourtNotCovered;
    }

    public UnitDTO setDepSportCourtNotCovered(Long depSportCourtNotCovered) {
        this.depSportCourtNotCovered = depSportCourtNotCovered;
        return this;
    }

    public Long getDepSportCourt() {
        return depSportCourt;
    }

    public UnitDTO setDepSportCourt(Long depSportCourt) {
        this.depSportCourt = depSportCourt;
        return this;
    }

    public Long getDepKitchen() {
        return depKitchen;
    }

    public UnitDTO setDepKitchen(Long depKitchen) {
        this.depKitchen = depKitchen;
        return this;
    }

    public Long getDepLibrary() {
        return depLibrary;
    }

    public UnitDTO setDepLibrary(Long depLibrary) {
        this.depLibrary = depLibrary;
        return this;
    }

    public Long getDepReadingRoom() {
        return depReadingRoom;
    }

    public UnitDTO setDepReadingRoom(Long depReadingRoom) {
        this.depReadingRoom = depReadingRoom;
        return this;
    }

    public Long getDepLibraryReadingRoom() {
        return depLibraryReadingRoom;
    }

    public UnitDTO setDepLibraryReadingRoom(Long depLibraryReadingRoom) {
        this.depLibraryReadingRoom = depLibraryReadingRoom;
        return this;
    }

    public Long getDepPlayground() {
        return depPlayground;
    }

    public UnitDTO setDepPlayground(Long depPlayground) {
        this.depPlayground = depPlayground;
        return this;
    }

    public Long getDepNurcery() {
        return depNurcery;
    }

    public UnitDTO setDepNurcery(Long depNurcery) {
        this.depNurcery = depNurcery;
        return this;
    }

    public Long getDepBathroomOutsideBuilding() {
        return depBathroomOutsideBuilding;
    }

    public UnitDTO setDepBathroomOutsideBuilding(Long depBathroomOutsideBuilding) {
        this.depBathroomOutsideBuilding = depBathroomOutsideBuilding;
        return this;
    }

    public Long getDepBathroomInsideBuilding() {
        return depBathroomInsideBuilding;
    }

    public UnitDTO setDepBathroomInsideBuilding(Long depBathroomInsideBuilding) {
        this.depBathroomInsideBuilding = depBathroomInsideBuilding;
        return this;
    }

    public Long getDepBathroomEI() {
        return depBathroomEI;
    }

    public UnitDTO setDepBathroomEI(Long depBathroomEI) {
        this.depBathroomEI = depBathroomEI;
        return this;
    }

    public Long getDepBathroomPNE() {
        return depBathroomPNE;
    }

    public UnitDTO setDepBathroomPNE(Long depBathroomPNE) {
        this.depBathroomPNE = depBathroomPNE;
        return this;
    }

    public Long getDepPNE() {
        return depPNE;
    }

    public UnitDTO setDepPNE(Long depPNE) {
        this.depPNE = depPNE;
        return this;
    }

    public Long getDepSecretary() {
        return depSecretary;
    }

    public UnitDTO setDepSecretary(Long depSecretary) {
        this.depSecretary = depSecretary;
        return this;
    }

    public Long getDepBathroomWithShower() {
        return depBathroomWithShower;
    }

    public UnitDTO setDepBathroomWithShower(Long depBathroomWithShower) {
        this.depBathroomWithShower = depBathroomWithShower;
        return this;
    }

    public Long getDepRefectory() {
        return depRefectory;
    }

    public UnitDTO setDepRefectory(Long depRefectory) {
        this.depRefectory = depRefectory;
        return this;
    }

    public Long getDepPantry() {
        return depPantry;
    }

    public UnitDTO setDepPantry(Long depPantry) {
        this.depPantry = depPantry;
        return this;
    }

    public Long getDepWarehouse() {
        return depWarehouse;
    }

    public UnitDTO setDepWarehouse(Long depWarehouse) {
        this.depWarehouse = depWarehouse;
        return this;
    }

    public Long getDepAuditorium() {
        return depAuditorium;
    }

    public UnitDTO setDepAuditorium(Long depAuditorium) {
        this.depAuditorium = depAuditorium;
        return this;
    }

    public Long getDepCourtyardCovered() {
        return depCourtyardCovered;
    }

    public UnitDTO setDepCourtyardCovered(Long depCourtyardCovered) {
        this.depCourtyardCovered = depCourtyardCovered;
        return this;
    }

    public Long getDepCourtyardNotCovered() {
        return depCourtyardNotCovered;
    }

    public UnitDTO setDepCourtyardNotCovered(Long depCourtyardNotCovered) {
        this.depCourtyardNotCovered = depCourtyardNotCovered;
        return this;
    }

    public Long getDepStudentAccommodation() {
        return depStudentAccommodation;
    }

    public UnitDTO setDepStudentAccommodation(Long depStudentAccommodation) {
        this.depStudentAccommodation = depStudentAccommodation;
        return this;
    }

    public Long getDepTeacherAccommodation() {
        return depTeacherAccommodation;
    }

    public UnitDTO setDepTeacherAccommodation(Long depTeacherAccommodation) {
        this.depTeacherAccommodation = depTeacherAccommodation;
        return this;
    }

    public Long getDepGreenArea() {
        return depGreenArea;
    }

    public UnitDTO setDepGreenArea(Long depGreenArea) {
        this.depGreenArea = depGreenArea;
        return this;
    }

    public Long getDepLaundry() {
        return depLaundry;
    }

    public UnitDTO setDepLaundry(Long depLaundry) {
        this.depLaundry = depLaundry;
        return this;
    }

    public Long getDepNoneOthers() {
        return depNoneOthers;
    }

    public UnitDTO setDepNoneOthers(Long depNoneOthers) {
        this.depNoneOthers = depNoneOthers;
        return this;
    }

    public Long getExNuExistingRooms() {
        return exNuExistingRooms;
    }

    public UnitDTO setExNuExistingRooms(Long exNuExistingRooms) {
        this.exNuExistingRooms = exNuExistingRooms;
        return this;
    }

    public Long getExNuUsedRooms() {
        return exNuUsedRooms;
    }

    public UnitDTO setExNuUsedRooms(Long exNuUsedRooms) {
        this.exNuUsedRooms = exNuUsedRooms;
        return this;
    }

    public Long getEquipTv() {
        return equipTv;
    }

    public UnitDTO setEquipTv(Long equipTv) {
        this.equipTv = equipTv;
        return this;
    }

    public Long getEquipVideotape() {
        return equipVideotape;
    }

    public UnitDTO setEquipVideotape(Long equipVideotape) {
        this.equipVideotape = equipVideotape;
        return this;
    }

    public Long getEquipDVD() {
        return equipDVD;
    }

    public UnitDTO setEquipDVD(Long equipDVD) {
        this.equipDVD = equipDVD;
        return this;
    }

    public Long getEquipParabolic() {
        return equipParabolic;
    }

    public UnitDTO setEquipParabolic(Long equipParabolic) {
        this.equipParabolic = equipParabolic;
        return this;
    }

    public Long getEquipCopyMachine() {
        return equipCopyMachine;
    }

    public UnitDTO setEquipCopyMachine(Long equipCopyMachine) {
        this.equipCopyMachine = equipCopyMachine;
        return this;
    }

    public Long getEquipOverheadProjector() {
        return equipOverheadProjector;
    }

    public UnitDTO setEquipOverheadProjector(Long equipOverheadProjector) {
        this.equipOverheadProjector = equipOverheadProjector;
        return this;
    }

    public Long getEquipPrinter() {
        return equipPrinter;
    }

    public UnitDTO setEquipPrinter(Long equipPrinter) {
        this.equipPrinter = equipPrinter;
        return this;
    }

    public Long getEquipMultifunctionPrinter() {
        return equipMultifunctionPrinter;
    }

    public UnitDTO setEquipMultifunctionPrinter(Long equipMultifunctionPrinter) {
        this.equipMultifunctionPrinter = equipMultifunctionPrinter;
        return this;
    }

    public Long getEquipStereo() {
        return equipStereo;
    }

    public UnitDTO setEquipStereo(Long equipStereo) {
        this.equipStereo = equipStereo;
        return this;
    }

    public Long getEquipDatashow() {
        return equipDatashow;
    }

    public UnitDTO setEquipDatashow(Long equipDatashow) {
        this.equipDatashow = equipDatashow;
        return this;
    }

    public Long getEquipFax() {
        return equipFax;
    }

    public UnitDTO setEquipFax(Long equipFax) {
        this.equipFax = equipFax;
        return this;
    }

    public Long getEquipPhoto() {
        return equipPhoto;
    }

    public UnitDTO setEquipPhoto(Long equipPhoto) {
        this.equipPhoto = equipPhoto;
        return this;
    }

    public Long getEquipComputer() {
        return equipComputer;
    }

    public UnitDTO setEquipComputer(Long equipComputer) {
        this.equipComputer = equipComputer;
        return this;
    }

    public Long getEquipNuTv() {
        return equipNuTv;
    }

    public UnitDTO setEquipNuTv(Long equipNuTv) {
        this.equipNuTv = equipNuTv;
        return this;
    }

    public Long getEquipNuVideotape() {
        return equipNuVideotape;
    }

    public UnitDTO setEquipNuVideotape(Long equipNuVideotape) {
        this.equipNuVideotape = equipNuVideotape;
        return this;
    }

    public Long getEquipNuDVD() {
        return equipNuDVD;
    }

    public UnitDTO setEquipNuDVD(Long equipNuDVD) {
        this.equipNuDVD = equipNuDVD;
        return this;
    }

    public Long getEquipNuParabolic() {
        return equipNuParabolic;
    }

    public UnitDTO setEquipNuParabolic(Long equipNuParabolic) {
        this.equipNuParabolic = equipNuParabolic;
        return this;
    }

    public Long getEquipNuCopyMachine() {
        return equipNuCopyMachine;
    }

    public UnitDTO setEquipNuCopyMachine(Long equipNuCopyMachine) {
        this.equipNuCopyMachine = equipNuCopyMachine;
        return this;
    }

    public Long getEquipNuOverheadProjector() {
        return equipNuOverheadProjector;
    }

    public UnitDTO setEquipNuOverheadProjector(Long equipNuOverheadProjector) {
        this.equipNuOverheadProjector = equipNuOverheadProjector;
        return this;
    }

    public Long getEquipNuPrinter() {
        return equipNuPrinter;
    }

    public UnitDTO setEquipNuPrinter(Long equipNuPrinter) {
        this.equipNuPrinter = equipNuPrinter;
        return this;
    }

    public Long getEquipNuMultifunctionPrinter() {
        return equipNuMultifunctionPrinter;
    }

    public UnitDTO setEquipNuMultifunctionPrinter(Long equipNuMultifunctionPrinter) {
        this.equipNuMultifunctionPrinter = equipNuMultifunctionPrinter;
        return this;
    }

    public Long getEquipNuStereo() {
        return equipNuStereo;
    }

    public UnitDTO setEquipNuStereo(Long equipNuStereo) {
        this.equipNuStereo = equipNuStereo;
        return this;
    }

    public Long getEquipNuDatashow() {
        return equipNuDatashow;
    }

    public UnitDTO setEquipNuDatashow(Long equipNuDatashow) {
        this.equipNuDatashow = equipNuDatashow;
        return this;
    }

    public Long getEquipNuFax() {
        return equipNuFax;
    }

    public UnitDTO setEquipNuFax(Long equipNuFax) {
        this.equipNuFax = equipNuFax;
        return this;
    }

    public Long getEquipNuPhoto() {
        return equipNuPhoto;
    }

    public UnitDTO setEquipNuPhoto(Long equipNuPhoto) {
        this.equipNuPhoto = equipNuPhoto;
        return this;
    }

    public Long getEquipNuComputer() {
        return equipNuComputer;
    }

    public UnitDTO setEquipNuComputer(Long equipNuComputer) {
        this.equipNuComputer = equipNuComputer;
        return this;
    }

    public Long getEquipComputerAdm() {
        return equipComputerAdm;
    }

    public UnitDTO setEquipComputerAdm(Long equipComputerAdm) {
        this.equipComputerAdm = equipComputerAdm;
        return this;
    }

    public Long getEquipComputerStudent() {
        return equipComputerStudent;
    }

    public UnitDTO setEquipComputerStudent(Long equipComputerStudent) {
        this.equipComputerStudent = equipComputerStudent;
        return this;
    }

    public Long getInternet() {
        return internet;
    }

    public UnitDTO setInternet(Long internet) {
        this.internet = internet;
        return this;
    }

    public Long getBroadbandInternet() {
        return broadbandInternet;
    }

    public UnitDTO setBroadbandInternet(Long broadbandInternet) {
        this.broadbandInternet = broadbandInternet;
        return this;
    }

    public Long getEmployees() {
        return employees;
    }

    public UnitDTO setEmployees(Long employees) {
        this.employees = employees;
        return this;
    }

    public Long getFood() {
        return food;
    }

    public UnitDTO setFood(Long food) {
        this.food = food;
        return this;
    }

    public Long getSpecializedEducationalService() {
        return specializedEducationalService;
    }

    public UnitDTO setSpecializedEducationalService(Long specializedEducationalService) {
        this.specializedEducationalService = specializedEducationalService;
        return this;
    }

    public Long getComplementaryActivity() {
        return complementaryActivity;
    }

    public UnitDTO setComplementaryActivity(Long complementaryActivity) {
        this.complementaryActivity = complementaryActivity;
        return this;
    }

    public Long getElementaryUnitOrganizedInCycles() {
        return elementaryUnitOrganizedInCycles;
    }

    public UnitDTO setElementaryUnitOrganizedInCycles(Long elementaryUnitOrganizedInCycles) {
        this.elementaryUnitOrganizedInCycles = elementaryUnitOrganizedInCycles;
        return this;
    }

    public Long getDifferentiatedUnitriLocation() {
        return differentiatedUnitriLocation;
    }

    public UnitDTO setDifferentiatedUnitriLocation(Long differentiatedUnitriLocation) {
        this.differentiatedUnitriLocation = differentiatedUnitriLocation;
        return this;
    }

    public Long getMdeQuilombolas() {
        return mdeQuilombolas;
    }

    public UnitDTO setMdeQuilombolas(Long mdeQuilombolas) {
        this.mdeQuilombolas = mdeQuilombolas;
        return this;
    }

    public Long getMdeIndigenous() {
        return mdeIndigenous;
    }

    public UnitDTO setMdeIndigenous(Long mdeIndigenous) {
        this.mdeIndigenous = mdeIndigenous;
        return this;
    }

    public Long getMdeNotUse() {
        return mdeNotUse;
    }

    public UnitDTO setMdeNotUse(Long mdeNotUse) {
        this.mdeNotUse = mdeNotUse;
        return this;
    }

    public Long getIndigenousEducation() {
        return indigenousEducation;
    }

    public UnitDTO setIndigenousEducation(Long indigenousEducation) {
        this.indigenousEducation = indigenousEducation;
        return this;
    }

    public Long getIndigenousLanguageName() {
        return indigenousLanguageName;
    }

    public UnitDTO setIndigenousLanguageName(Long indigenousLanguageName) {
        this.indigenousLanguageName = indigenousLanguageName;
        return this;
    }

    public Long getIndigenousLanguageCod() {
        return indigenousLanguageCod;
    }

    public UnitDTO setIndigenousLanguageCod(Long indigenousLanguageCod) {
        this.indigenousLanguageCod = indigenousLanguageCod;
        return this;
    }

    public Long getBrazilLiterate() {
        return brazilLiterate;
    }

    public UnitDTO setBrazilLiterate(Long brazilLiterate) {
        this.brazilLiterate = brazilLiterate;
        return this;
    }

    public Long getOpensOnWeekends() {
        return opensOnWeekends;
    }

    public UnitDTO setOpensOnWeekends(Long opensOnWeekends) {
        this.opensOnWeekends = opensOnWeekends;
        return this;
    }

    public Long getAlternateFormation() {
        return alternateFormation;
    }

    public UnitDTO setAlternateFormation(Long alternateFormation) {
        this.alternateFormation = alternateFormation;
        return this;
    }

    public Long getDidacticPedagogicalPresential() {
        return didacticPedagogicalPresential;
    }

    public UnitDTO setDidacticPedagogicalPresential(Long didacticPedagogicalPresential) {
        this.didacticPedagogicalPresential = didacticPedagogicalPresential;
        return this;
    }

    public Long getDidacticPedagogicalSemiPresential() {
        return didacticPedagogicalSemiPresential;
    }

    public UnitDTO setDidacticPedagogicalSemiPresential(Long didacticPedagogicalSemiPresential) {
        this.didacticPedagogicalSemiPresential = didacticPedagogicalSemiPresential;
        return this;
    }

    public Long getDidacticPedagogicalEAD() {
        return didacticPedagogicalEAD;
    }

    public UnitDTO setDidacticPedagogicalEAD(Long didacticPedagogicalEAD) {
        this.didacticPedagogicalEAD = didacticPedagogicalEAD;
        return this;
    }

    public Long getExclusiveSpecial() {
        return exclusiveSpecial;
    }

    public UnitDTO setExclusiveSpecial(Long exclusiveSpecial) {
        this.exclusiveSpecial = exclusiveSpecial;
        return this;
    }

    public Long getEdRegular() {
        return edRegular;
    }

    public UnitDTO setEdRegular(Long edRegular) {
        this.edRegular = edRegular;
        return this;
    }

    public Long getEdEJA() {
        return edEJA;
    }

    public UnitDTO setEdEJA(Long edEJA) {
        this.edEJA = edEJA;
        return this;
    }

    public Long getEdProfessional() {
        return edProfessional;
    }

    public UnitDTO setEdProfessional(Long edProfessional) {
        this.edProfessional = edProfessional;
        return this;
    }

    public Long getCoNursery() {
        return coNursery;
    }

    public UnitDTO setCoNursery(Long coNursery) {
        this.coNursery = coNursery;
        return this;
    }

    public Long getCoPre() {
        return coPre;
    }

    public UnitDTO setCoPre(Long coPre) {
        this.coPre = coPre;
        return this;
    }

    public Long getCoFundAI() {
        return coFundAI;
    }

    public UnitDTO setCoFundAI(Long coFundAI) {
        this.coFundAI = coFundAI;
        return this;
    }

    public Long getCoFundAF() {
        return coFundAF;
    }

    public UnitDTO setCoFundAF(Long coFundAF) {
        this.coFundAF = coFundAF;
        return this;
    }

    public Long getCoMedioMP() {
        return coMedioMP;
    }

    public UnitDTO setCoMedioMP(Long coMedioMP) {
        this.coMedioMP = coMedioMP;
        return this;
    }

    public Long getCoMedioIN() {
        return coMedioIN;
    }

    public UnitDTO setCoMedioIN(Long coMedioIN) {
        this.coMedioIN = coMedioIN;
        return this;
    }

    public Long getCoMedioNo() {
        return coMedioNo;
    }

    public UnitDTO setCoMedioNo(Long coMedioNo) {
        this.coMedioNo = coMedioNo;
        return this;
    }

    public Long getExSpNursery() {
        return exSpNursery;
    }

    public UnitDTO setExSpNursery(Long exSpNursery) {
        this.exSpNursery = exSpNursery;
        return this;
    }

    public Long getExSpPre() {
        return exSpPre;
    }

    public UnitDTO setExSpPre(Long exSpPre) {
        this.exSpPre = exSpPre;
        return this;
    }

    public Long getExSpFundAI() {
        return exSpFundAI;
    }

    public UnitDTO setExSpFundAI(Long exSpFundAI) {
        this.exSpFundAI = exSpFundAI;
        return this;
    }

    public Long getExSpFundAF() {
        return exSpFundAF;
    }

    public UnitDTO setExSpFundAF(Long exSpFundAF) {
        this.exSpFundAF = exSpFundAF;
        return this;
    }

    public Long getExSpMedioMP() {
        return exSpMedioMP;
    }

    public UnitDTO setExSpMedioMP(Long exSpMedioMP) {
        this.exSpMedioMP = exSpMedioMP;
        return this;
    }

    public Long getExSpMedioIN() {
        return exSpMedioIN;
    }

    public UnitDTO setExSpMedioIN(Long exSpMedioIN) {
        this.exSpMedioIN = exSpMedioIN;
        return this;
    }

    public Long getExSpMedioNo() {
        return exSpMedioNo;
    }

    public UnitDTO setExSpMedioNo(Long exSpMedioNo) {
        this.exSpMedioNo = exSpMedioNo;
        return this;
    }

    public Long getCoEJAFund() {
        return coEJAFund;
    }

    public UnitDTO setCoEJAFund(Long coEJAFund) {
        this.coEJAFund = coEJAFund;
        return this;
    }

    public Long getCoEJAMedio() {
        return coEJAMedio;
    }

    public UnitDTO setCoEJAMedio(Long coEJAMedio) {
        this.coEJAMedio = coEJAMedio;
        return this;
    }

    public Long getCoEJAProf() {
        return coEJAProf;
    }

    public UnitDTO setCoEJAProf(Long coEJAProf) {
        this.coEJAProf = coEJAProf;
        return this;
    }

    public Long getExSpEJAFund() {
        return exSpEJAFund;
    }

    public UnitDTO setExSpEJAFund(Long exSpEJAFund) {
        this.exSpEJAFund = exSpEJAFund;
        return this;
    }

    public Long getExSpEJAMedio() {
        return exSpEJAMedio;
    }

    public UnitDTO setExSpEJAMedio(Long exSpEJAMedio) {
        this.exSpEJAMedio = exSpEJAMedio;
        return this;
    }

    public Long getExSpEJAProf() {
        return exSpEJAProf;
    }

    public UnitDTO setExSpEJAProf(Long exSpEJAProf) {
        this.exSpEJAProf = exSpEJAProf;
        return this;
    }

    public Long getCoProf() {
        return coProf;
    }

    public UnitDTO setCoProf(Long coProf) {
        this.coProf = coProf;
        return this;
    }

    public Long getExSpProf() {
        return exSpProf;
    }

    public UnitDTO setExSpProf(Long exSpProf) {
        this.exSpProf = exSpProf;
        return this;
    }
}
