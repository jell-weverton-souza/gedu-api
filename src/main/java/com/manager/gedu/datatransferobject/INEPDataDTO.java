package com.manager.gedu.datatransferobject;

import com.manager.gedu.abstracts.AbstractDataTransferObject;

/**
 * @author Weverton Souza.
 * Created on 27/07/2019
 */
public class INEPDataDTO extends AbstractDataTransferObject {
    private String codeRegister;
    private String codeUnit;
    private String codeClassUnit;
    private String codeStudentUnit;
    private String codeProfessionalUnit;

    public INEPDataDTO() {}

    public String getCodeRegister() {
        return codeRegister;
    }

    public INEPDataDTO setCodeRegister(String codeRegister) {
        this.codeRegister = codeRegister;
        return this;
    }

    public String getCodeUnit() {
        return codeUnit;
    }

    public INEPDataDTO setCodeUnit(String codeUnit) {
        this.codeUnit = codeUnit;
        return this;
    }

    public String getCodeClassUnit() {
        return codeClassUnit;
    }

    public INEPDataDTO setCodeClassUnit(String codeClassUnit) {
        this.codeClassUnit = codeClassUnit;
        return this;
    }

    public String getCodeStudentUnit() {
        return codeStudentUnit;
    }

    public INEPDataDTO setCodeStudentUnit(String codeStudentUnit) {
        this.codeStudentUnit = codeStudentUnit;
        return this;
    }

    public String getCodeProfessionalUnit() {
        return codeProfessionalUnit;
    }

    public INEPDataDTO setCodeProfessionalUnit(String codeProfessionalUnit) {
        this.codeProfessionalUnit = codeProfessionalUnit;
        return this;
    }
}
