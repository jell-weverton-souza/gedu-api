package com.manager.gedu.datatransferobject.classroom;

import com.manager.gedu.abstracts.AbstractDataTransferObject;
import com.manager.gedu.datatransferobject.INEPDataDTO;
import com.manager.gedu.domain.classroom.ClassRoomRegistration;

import java.util.List;

/**
 * @author Weverton Souza.
 * Created on 01/08/2019
 */
public class ClassRoomDTO extends AbstractDataTransferObject {
    private INEPDataDTO inepData;
    private String classRoomName;
    private ClassRoomRegistration serviceType;
    private ClassRoomRegistration participatingInnovativeHighSchoolMoreEducation;
    private List<ClassRoomRegistrationDTO> additionalActivityType;
    private ClassRoomRegistrationDTO modality;
    private Integer teachingStage;
    private Integer codeCourseProfessionalEducation;
    private List<ClassRoomRegistrationDTO> subjects;
    private Boolean ClassroomWithoutUnitProfessional;

    public ClassRoomDTO() {}

    public INEPDataDTO getInepData() {
        return inepData;
    }

    public void setInepData(INEPDataDTO inepData) {
        this.inepData = inepData;
    }

    public String getClassRoomName() {
        return classRoomName;
    }

    public void setClassRoomName(String classRoomName) {
        this.classRoomName = classRoomName;
    }

    public ClassRoomRegistration getServiceType() {
        return serviceType;
    }

    public void setServiceType(ClassRoomRegistration serviceType) {
        this.serviceType = serviceType;
    }

    public ClassRoomRegistration getParticipatingInnovativeHighSchoolMoreEducation() {
        return participatingInnovativeHighSchoolMoreEducation;
    }

    public void setParticipatingInnovativeHighSchoolMoreEducation(ClassRoomRegistration participatingInnovativeHighSchoolMoreEducation) {
        this.participatingInnovativeHighSchoolMoreEducation = participatingInnovativeHighSchoolMoreEducation;
    }

    public List<ClassRoomRegistrationDTO> getAdditionalActivityType() {
        return additionalActivityType;
    }

    public void setAdditionalActivityType(List<ClassRoomRegistrationDTO> additionalActivityType) {
        this.additionalActivityType = additionalActivityType;
    }

    public ClassRoomRegistrationDTO getModality() {
        return modality;
    }

    public void setModality(ClassRoomRegistrationDTO modality) {
        this.modality = modality;
    }

    public Integer getTeachingStage() {
        return teachingStage;
    }

    public void setTeachingStage(Integer teachingStage) {
        this.teachingStage = teachingStage;
    }

    public Integer getCodeCourseProfessionalEducation() {
        return codeCourseProfessionalEducation;
    }

    public void setCodeCourseProfessionalEducation(Integer codeCourseProfessionalEducation) {
        this.codeCourseProfessionalEducation = codeCourseProfessionalEducation;
    }

    public List<ClassRoomRegistrationDTO> getSubjects() {
        return subjects;
    }

    public void setSubjects(List<ClassRoomRegistrationDTO> subjects) {
        this.subjects = subjects;
    }

    public Boolean getClassroomWithoutUnitProfessional() {
        return ClassroomWithoutUnitProfessional;
    }

    public void setClassroomWithoutUnitProfessional(Boolean classroomWithoutUnitProfessional) {
        ClassroomWithoutUnitProfessional = classroomWithoutUnitProfessional;
    }
}
