package com.manager.gedu.datatransferobject.classroom;

import com.manager.gedu.abstracts.AbstractDataTransferObject;
import com.manager.gedu.abstracts.AbstractDomain;
import org.springframework.data.mongodb.core.mapping.Document;

import java.sql.Time;
import java.util.List;

/**
 * @author Weverton Souza.
 * Created on 01/08/2019
 */
public class ClassRoomScheduleDTO extends AbstractDataTransferObject {
    private Time startTime;
    private Time endTime;
    private List<String> daysOfWeek;

    public ClassRoomScheduleDTO() {}

    public Time getStartTime() {
        return startTime;
    }

    public void setStartTime(Time startTime) {
        this.startTime = startTime;
    }

    public Time getEndTime() {
        return endTime;
    }

    public void setEndTime(Time endTime) {
        this.endTime = endTime;
    }

    public List<String> getDaysOfWeek() {
        return daysOfWeek;
    }

    public void setDaysOfWeek(List<String> daysOfWeek) {
        this.daysOfWeek = daysOfWeek;
    }
}
