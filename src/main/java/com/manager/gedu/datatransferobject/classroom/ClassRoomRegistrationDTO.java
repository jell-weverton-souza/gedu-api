package com.manager.gedu.datatransferobject.classroom;

import com.manager.gedu.abstracts.AbstractDataTransferObject;
import com.manager.gedu.abstracts.AbstractDomain;

import java.util.List;

/**
 * @author Weverton Souza.
 * Created on 01/08/2019
 */
public class ClassRoomRegistrationDTO extends AbstractDataTransferObject {
    private String registerType;
    private String registerName;
    private Integer registerCode;
    private String registerDescription;
    private List<String> roles;

    public ClassRoomRegistrationDTO() {}

    public String getRegisterType() {
        return registerType;
    }

    public void setRegisterType(String registerType) {
        this.registerType = registerType;
    }

    public String getRegisterName() {
        return registerName;
    }

    public void setRegisterName(String registerName) {
        this.registerName = registerName;
    }

    public Integer getRegisterCode() {
        return registerCode;
    }

    public void setRegisterCode(Integer registerCode) {
        this.registerCode = registerCode;
    }

    public String getRegisterDescription() {
        return registerDescription;
    }

    public void setRegisterDescription(String registerDescription) {
        this.registerDescription = registerDescription;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }
}
