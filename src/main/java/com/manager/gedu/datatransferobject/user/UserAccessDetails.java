package com.manager.gedu.datatransferobject.user;

public class UserAccessDetails {
    private String userCode;
    private String email;
    private String password;

    public UserAccessDetails() {}

    public String getUserCode() {
        return userCode;
    }

    public UserAccessDetails setUserCode(String userCode) {
        this.userCode = userCode;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public UserAccessDetails setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public UserAccessDetails setPassword(String password) {
        this.password = password;
        return this;
    }
}
