package com.manager.gedu.datatransferobject.user;

import com.manager.gedu.abstracts.AbstractDataTransferObject;
import com.manager.gedu.enums.DegreeOfKinship;
import org.springframework.data.mongodb.core.mapping.DBRef;

public class FamilyMemberDTO extends AbstractDataTransferObject {
    @DBRef
    private PersonDTO memberData;
    private DegreeOfKinship degreeOfKinship;

    public FamilyMemberDTO() {}

    public PersonDTO getMemberData() {
        return memberData;
    }

    public FamilyMemberDTO setMemberData(PersonDTO memberData) {
        this.memberData = memberData;
        return this;
    }

    public DegreeOfKinship getDegreeOfKinship() {
        return degreeOfKinship;
    }

    public FamilyMemberDTO setDegreeOfKinship(DegreeOfKinship degreeOfKinship) {
        this.degreeOfKinship = degreeOfKinship;
        return this;
    }
}
