package com.manager.gedu.datatransferobject.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.manager.gedu.abstracts.AbstractDataTransferObject;
import com.manager.gedu.enums.Roles;

import java.util.Set;

/**
 * @author Weverton Souza.
 * Created on 28/06/19
 */
public class UserAccessDTO extends AbstractDataTransferObject {
    private String userCode;
    private String email;
    @JsonIgnore
    private String password;
    private boolean credentialsNonExpired;
    private boolean accountNonLocked;
    private boolean accountNonExpired;
    private boolean enabled;
    private String typeAuthorization;
    private String token;
    private Set<Roles> roles;

    public UserAccessDTO() {}

    public String getUserCode() {
        return userCode;
    }

    public UserAccessDTO setUserCode(final String userCode) {
        this.userCode = userCode;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public UserAccessDTO setEmail(final String email) {
        this.email = email;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public UserAccessDTO setPassword(final String password) {
        this.password = password;
        return this;
    }

    public boolean isCredentialsNonExpired() {
        return credentialsNonExpired;
    }

    public UserAccessDTO setCredentialsNonExpired(final boolean credentialsNonExpired) {
        this.credentialsNonExpired = credentialsNonExpired;
        return this;
    }

    public boolean isAccountNonLocked() {
        return accountNonLocked;
    }

    public UserAccessDTO setAccountNonLocked(final boolean accountNonLocked) {
        this.accountNonLocked = accountNonLocked;
        return this;
    }

    public boolean isAccountNonExpired() {
        return accountNonExpired;
    }

    public UserAccessDTO setAccountNonExpired(final boolean accountNonExpired) {
        this.accountNonExpired = accountNonExpired;
        return this;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public UserAccessDTO setEnabled(final boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    public String getTypeAuthorization() {
        return typeAuthorization;
    }

    public UserAccessDTO setTypeAuthorization(final String typeAuthorization) {
        this.typeAuthorization = typeAuthorization;
        return this;
    }

    public String getToken() {
        return token;
    }

    public UserAccessDTO setToken(final String token) {
        this.token = token;
        return this;
    }

    public Set<Roles> getRoles() {
        return roles;
    }

    public UserAccessDTO setRoles(final Set<Roles> roles) {
        this.roles = roles;
        return this;
    }
}
