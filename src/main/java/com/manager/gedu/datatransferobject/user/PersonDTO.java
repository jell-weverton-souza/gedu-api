package com.manager.gedu.datatransferobject.user;

import com.manager.gedu.abstracts.AbstractDataTransferObject;

public class PersonDTO extends AbstractDataTransferObject {
    private String firstName;
    private String lastName;
    private String placeOfBirth;
    private String dateOfBirth;
    private String gender;
    private String cpf;
    private String rg;

    public PersonDTO() {}

    public String getFirstName() {
        return firstName;
    }

    public PersonDTO setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public PersonDTO setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public String getPlaceOfBirth() {
        return placeOfBirth;
    }

    public PersonDTO setPlaceOfBirth(String placeOfBirth) {
        this.placeOfBirth = placeOfBirth;
        return this;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public PersonDTO setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
        return this;
    }

    public String getGender() {
        return gender;
    }

    public PersonDTO setGender(String gender) {
        this.gender = gender;
        return this;
    }

    public String getCpf() {
        return cpf;
    }

    public PersonDTO setCpf(String cpf) {
        this.cpf = cpf;
        return this;
    }

    public String getRg() {
        return rg;
    }

    public PersonDTO setRg(String rg) {
        this.rg = rg;
        return this;
    }
}
