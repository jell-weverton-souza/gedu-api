package com.manager.gedu.datatransferobject.user;

import com.manager.gedu.abstracts.AbstractDataTransferObject;
import com.manager.gedu.datatransferobject.address.AddressDTO;
import com.manager.gedu.enums.Roles;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.util.List;

/**
 * @author Weverton Souza.
 * Created on 28/06/19
 */
public class UserDTO extends AbstractDataTransferObject {
    private String userCode;
    private String email;
    @DBRef
    private PersonDTO personalData;
    private List<FamilyMemberDTO> familyData;
    @DBRef
    private List<AddressDTO> addresses;
    private List<Roles> roles;

    public UserDTO() {}

    public String getUserCode() {
        return userCode;
    }

    public UserDTO setUserCode(String userCode) {
        this.userCode = userCode;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public UserDTO setEmail(String email) {
        this.email = email;
        return this;
    }

    public PersonDTO getPersonalData() {
        return personalData;
    }

    public UserDTO setPersonalData(PersonDTO personalData) {
        this.personalData = personalData;
        return this;
    }

    public List<AddressDTO> getAddresses() {
        return addresses;
    }

    public UserDTO setAddresses(List<AddressDTO> addresses) {
        this.addresses = addresses;
        return this;
    }

    public List<Roles> getRoles() {
        return roles;
    }

    public UserDTO setRoles(List<Roles> roles) {
        this.roles = roles;
        return this;
    }

    public List<FamilyMemberDTO> getFamilyData() {
        return familyData;
    }

    public UserDTO setFamilyData(List<FamilyMemberDTO> familyData) {
        this.familyData = familyData;
        return this;
    }
}
