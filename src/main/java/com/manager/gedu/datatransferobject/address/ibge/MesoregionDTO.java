package com.manager.gedu.datatransferobject.address.ibge;

import com.google.gson.annotations.SerializedName;
import com.manager.gedu.interfaces.IClientDomain;

public class MesoregionDTO implements IClientDomain {
    private String id;
    @SerializedName(value = "nome")
    private String name;
    @SerializedName(value = "UF")
    private UFDTO UF;

    public MesoregionDTO() {}

    public String getId() {
        return id;
    }

    public MesoregionDTO setId(String id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public MesoregionDTO setName(String name) {
        this.name = name;
        return this;
    }

    public UFDTO getUF() {
        return UF;
    }

    public MesoregionDTO setUF(UFDTO UF) {
        this.UF = UF;
        return this;
    }
}
