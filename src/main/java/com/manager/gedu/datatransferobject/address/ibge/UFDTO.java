package com.manager.gedu.datatransferobject.address.ibge;

import com.google.gson.annotations.SerializedName;
import com.manager.gedu.interfaces.IClientDomain;

public class UFDTO implements IClientDomain {
    private String id;
    @SerializedName(value = "nome")
    private String name;
    @SerializedName(value = "sigla")
    private String initials;
    @SerializedName(value = "regiao")
    private RegionDTO regionDTO;

    public UFDTO() {}

    public String getId() {
        return id;
    }

    public UFDTO setId(final String id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public UFDTO setName(final String name) {
        this.name = name;
        return this;
    }

    public String getInitials() {
        return initials;
    }

    public UFDTO setInitials(final String initials) {
        this.initials = initials;
        return this;
    }

    public RegionDTO getRegionDTO() {
        return regionDTO;
    }

    public UFDTO setRegionDTO(final RegionDTO regionDTO) {
        this.regionDTO = regionDTO;
        return this;
    }
}
