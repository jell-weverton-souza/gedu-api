package com.manager.gedu.datatransferobject.address.ibge;

import com.google.gson.annotations.SerializedName;
import com.manager.gedu.interfaces.IClientDomain;

public class RegionDTO implements IClientDomain {
    private Integer id;
    @SerializedName(value = "sigla")
    private String initials;
    @SerializedName(value = "nome")
    private String name;

    public RegionDTO() {}

    public Integer getId() {
        return id;
    }

    public RegionDTO setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getInitials() {
        return initials;
    }

    public RegionDTO setInitials(String initials) {
        this.initials = initials;
        return this;
    }

    public String getName() {
        return name;
    }

    public RegionDTO setName(String name) {
        this.name = name;
        return this;
    }
}
