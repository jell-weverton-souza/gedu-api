package com.manager.gedu.datatransferobject.address;

import com.manager.gedu.abstracts.AbstractDataTransferObject;

/**
 * @author Weverton Souza.
 * Created on 26/06/19
 */
public class AddressDTO extends AbstractDataTransferObject {
    private Double latitude;
    private Double longitude;
    private String zipCode;
    private String address;
    private String addressNumber;
    private String addressComplement;
    private String neighborhood;
    private String country;
    private String state;
    private String city;
    private String district;

    public AddressDTO() { }

    public Double getLatitude() {
        return latitude;
    }

    public AddressDTO setLatitude(final Double latitude) {
        this.latitude = latitude;
        return this;
    }

    public Double getLongitude() {
        return longitude;
    }

    public AddressDTO setLongitude(final Double longitude) {
        this.longitude = longitude;
        return this;
    }

    public String getZipCode() {
        return zipCode;
    }

    public AddressDTO setZipCode(final String zipCode) {
        this.zipCode = zipCode;
        return this;
    }

    public String getAddress() {
        return address;
    }

    public AddressDTO setAddress(final String address) {
        this.address = address;
        return this;
    }

    public String getAddressNumber() {
        return addressNumber;
    }

    public AddressDTO setAddressNumber(final String addressNumber) {
        this.addressNumber = addressNumber;
        return this;
    }

    public String getAddressComplement() {
        return addressComplement;
    }

    public AddressDTO setAddressComplement(final String addressComplement) {
        this.addressComplement = addressComplement;
        return this;
    }

    public String getNeighborhood() {
        return neighborhood;
    }

    public AddressDTO setNeighborhood(final String neighborhood) {
        this.neighborhood = neighborhood;
        return this;
    }

    public String getCountry() {
        return country;
    }

    public AddressDTO setCountry(final String country) {
        this.country = country;
        return this;
    }

    public String getState() {
        return state;
    }

    public AddressDTO setState(final String state) {
        this.state = state;
        return this;
    }

    public String getCity() {
        return city;
    }

    public AddressDTO setCity(final String city) {
        this.city = city;
        return this;
    }

    public String getDistrict() {
        return district;
    }

    public AddressDTO setDistrict(final String district) {
        this.district = district;
        return this;
    }
}
