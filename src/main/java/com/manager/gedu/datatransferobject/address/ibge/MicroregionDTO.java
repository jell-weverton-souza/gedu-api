package com.manager.gedu.datatransferobject.address.ibge;

import com.google.gson.annotations.SerializedName;
import com.manager.gedu.interfaces.IClientDomain;

public class MicroregionDTO implements IClientDomain {
    private String id;
    @SerializedName(value = "nome")
    private String name;
    @SerializedName(value = "mesorregiao")
    private MesoregionDTO mesoregion;

    private MicroregionDTO() {}

    public String getId() {
        return id;
    }

    public MicroregionDTO setId(String id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public MicroregionDTO setName(String name) {
        this.name = name;
        return this;
    }

    public MesoregionDTO getMesoregion() {
        return mesoregion;
    }

    public MicroregionDTO setMesoregion(MesoregionDTO mesoregion) {
        this.mesoregion = mesoregion;
        return this;
    }
}
