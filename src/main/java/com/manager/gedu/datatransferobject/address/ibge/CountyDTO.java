package com.manager.gedu.datatransferobject.address.ibge;

import com.google.gson.annotations.SerializedName;
import com.manager.gedu.interfaces.IClientDomain;

public class CountyDTO implements IClientDomain {
    private String id;
    @SerializedName(value = "nome")
    private String name;
    @SerializedName(value = "microrregiao")
    private MicroregionDTO microregion;

    public CountyDTO() {}

    public String getId() {
        return id;
    }

    public CountyDTO setId(String id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public CountyDTO setName(String name) {
        this.name = name;
        return this;
    }

    public MicroregionDTO getMicroregion() {
        return microregion;
    }

    public CountyDTO setMicroregion(MicroregionDTO microregion) {
        this.microregion = microregion;
        return this;
    }
}
