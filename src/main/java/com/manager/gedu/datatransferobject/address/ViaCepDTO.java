package com.manager.gedu.datatransferobject.address;

import com.google.gson.annotations.SerializedName;
import com.manager.gedu.interfaces.IClientDomain;

/**
 * @author Weverton Souza.
 * Created on 27/06/19
 */
public class ViaCepDTO implements IClientDomain {
    @SerializedName(value="cep")
    private String zipCode;
    @SerializedName(value="logradouro")
    private String publicPlace;
    @SerializedName(value="complemento")
    private String complement;
    @SerializedName(value="bairro")
    private String neighborhood;
    @SerializedName(value="localidade")
    private String city;
    @SerializedName(value="uf")
    private String state;
    @SerializedName(value="unidade")
    private String unit;
    private String ibge;
    private String gia;

    public ViaCepDTO() { }

    public String getZipCode() {
        return zipCode;
    }

    public ViaCepDTO setZipCode(final String zipCode) {
        this.zipCode = zipCode;
        return this;
    }

    public String getPublicPlace() {
        return publicPlace;
    }

    public ViaCepDTO setPublicPlace(final String publicPlace) {
        this.publicPlace = publicPlace;
        return this;
    }

    public String getComplement() {
        return complement;
    }

    public ViaCepDTO setComplement(final String complement) {
        this.complement = complement;
        return this;
    }

    public String getNeighborhood() {
        return neighborhood;
    }

    public ViaCepDTO setNeighborhood(final String neighborhood) {
        this.neighborhood = neighborhood;
        return this;
    }

    public String getCity() {
        return city;
    }

    public ViaCepDTO setCity(final String city) {
        this.city = city;
        return this;
    }

    public String getState() {
        return state;
    }

    public ViaCepDTO setState(final String state) {
        this.state = state;
        return this;
    }

    public String getUnit() {
        return unit;
    }

    public ViaCepDTO setUnit(final String unit) {
        this.unit = unit;
        return this;
    }

    public String getIbge() {
        return ibge;
    }

    public ViaCepDTO setIbge(final String ibge) {
        this.ibge = ibge;
        return this;
    }

    public String getGia() {
        return gia;
    }

    public ViaCepDTO setGia(final String gia) {
        this.gia = gia;
        return this;
    }
}