package com.manager.gedu.repository;

import com.manager.gedu.domain.address.Address;
import com.manager.gedu.interfaces.IRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Weverton Souza.
 * Created on 28/06/19
 */
@Repository
public interface IAddressRepository extends IRepository<Address, String> {
}
