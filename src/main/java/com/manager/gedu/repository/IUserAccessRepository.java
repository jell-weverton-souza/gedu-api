package com.manager.gedu.repository;

import com.manager.gedu.domain.user.UserAccess;
import com.manager.gedu.interfaces.IRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * @author Weverton Souza.
 * Created on 28/06/19
 */
@Repository
public interface IUserAccessRepository extends IRepository<UserAccess, String> {
    Optional<UserAccess> findByUserCode(String userCode);
    Optional<UserAccess> findByEmail(String email);
}
