package com.manager.gedu.repository;

import com.manager.gedu.domain.user.User;
import com.manager.gedu.interfaces.IRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Weverton Souza.
 * Created on 28/06/19
 */
@Repository
public interface IUserRepository extends IRepository<User, String> {
}
