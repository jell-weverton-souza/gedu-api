package com.manager.gedu.repository;

import com.manager.gedu.domain.gradesubject.Grade;
import com.manager.gedu.interfaces.IRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Weverton Souza.
 * Created on 29/08/19
 */
@Repository
public interface IGradeRepository extends IRepository<Grade, String> {
}
