package com.manager.gedu.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author Weverton Souza.
 * Created on 27/06/19
 */
@Configuration
@EnableConfigurationProperties({PropConfiguration.class})
public class AppConfiguration {
    @Autowired
    private PropConfiguration configuration;

    public PropConfiguration getMyProperties() {
        return configuration;
    }

    public void setMyProperties(PropConfiguration configuration) {
        this.configuration = configuration;
    }
}
