package com.manager.gedu.abstracts;

import com.manager.gedu.enums.GeduHttpStatus;
import com.manager.gedu.generic.GenericResponse;
import com.manager.gedu.interfaces.IDataTransferObject;
import com.manager.gedu.interfaces.IResource;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.Serializable;

import static com.manager.gedu.api.GeduConstants.*;

/**
 * @author Weverton Souza.
 * Created on 16/06/19
 */
public abstract class AbstractResource<T extends AbstractDataTransferObject, K extends Serializable>
        implements IResource<T, K> {
    protected final Logger log = LoggerFactory.getLogger(getClass());
    protected final AbstractService<T, K> service;

    protected AbstractResource(AbstractService<T, K> service) {
        this.service = service;
    }

    @Override
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation(value = RESOURCE_OPERATION_CREATE, response = GenericResponse.class)
    @ApiResponses( value = {
            @ApiResponse(code = 201, message = HTTP_MESSAGE_201, response = GenericResponse.class),
            @ApiResponse(code = 401, message = HTTP_MESSAGE_401, response = GenericResponse.class),
            @ApiResponse(code = 403, message = HTTP_MESSAGE_403, response = GenericResponse.class),
            @ApiResponse(code = 500, message = HTTP_MESSAGE_403, response = GenericResponse.class),
    })
    public GenericResponse<?> save(@Valid @RequestBody T resource) {
        IDataTransferObject dataTransferObject = service.saveOrUpdate(resource);

        return  new GenericResponse<>(
                dataTransferObject,
                HttpStatus.CREATED.value(),
                HttpStatus.CREATED,
                GeduHttpStatus.CREATED.getMessage());
    }

    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = RESOURCE_OPERATION_UPDATE, response = GenericResponse.class)
    @ApiResponses( value = {
            @ApiResponse(code = 200, message = HTTP_MESSAGE_200, response = GenericResponse.class),
            @ApiResponse(code = 401, message = HTTP_MESSAGE_401, response = GenericResponse.class),
            @ApiResponse(code = 403, message = HTTP_MESSAGE_403, response = GenericResponse.class),
            @ApiResponse(code = 404, message = HTTP_MESSAGE_404, response = GenericResponse.class)
    })
    public GenericResponse update(@Valid @RequestBody T resource) {
        IDataTransferObject dataTransferObject = this.service.saveOrUpdate(resource);

        return new GenericResponse<>(
                dataTransferObject,
                HttpStatus.CREATED.value(),
                HttpStatus.CREATED,
                GeduHttpStatus.CREATED.getMessage());
    }

    @Override
    @GetMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = RESOURCE_OPERATION_FIND_BY_ID, response=GenericResponse.class)
    @ApiResponses( value = {
            @ApiResponse(code = 200, message = HTTP_MESSAGE_200, response = GenericResponse.class),
            @ApiResponse(code = 401, message = HTTP_MESSAGE_401, response = GenericResponse.class),
            @ApiResponse(code = 403, message = HTTP_MESSAGE_403, response = GenericResponse.class),
            @ApiResponse(code = 404, message = HTTP_MESSAGE_404, response = GenericResponse.class)
    })
    public GenericResponse findById(@PathVariable K id) {
        IDataTransferObject dataTransferObject = this.service.findById(id);

        return new GenericResponse<>(
                dataTransferObject,
                HttpStatus.OK.value(),
                HttpStatus.OK,
                GeduHttpStatus.OK.getMessage());
    }

    @Override
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = RESOURCE_OPERATION_FIND_ALL, response=GenericResponse.class)
    @ApiResponses( value = {
            @ApiResponse(code = 200, message = HTTP_MESSAGE_200, responseContainer="List", response = GenericResponse.class),
            @ApiResponse(code = 401, message = HTTP_MESSAGE_401, response = GenericResponse.class),
            @ApiResponse(code = 403, message = HTTP_MESSAGE_403, response = GenericResponse.class)
    })
    public Page<IDataTransferObject> findAll(Pageable pageable) {
        return service.findAll(pageable);
    }

    @Override
    @DeleteMapping
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = RESOURCE_OPERATION_DELETE, response = GenericResponse.class)
    @ApiResponses( value = {
            @ApiResponse(code = 200, message = HTTP_MESSAGE_200, response = GenericResponse.class),
            @ApiResponse(code = 401, message = HTTP_MESSAGE_401, response = GenericResponse.class),
            @ApiResponse(code = 403, message = HTTP_MESSAGE_403, response = GenericResponse.class),
            @ApiResponse(code = 404, message = HTTP_MESSAGE_404, response = GenericResponse.class)
    })
    public void delete(@PathVariable K id) {
        this.service.delete(id);
    }
}
