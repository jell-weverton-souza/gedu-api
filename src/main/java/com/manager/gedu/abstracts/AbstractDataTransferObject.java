package com.manager.gedu.abstracts;

import com.manager.gedu.interfaces.IDataTransferObject;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;
import java.util.Objects;

import static com.manager.gedu.api.DataTranferObjectConstants.API_MESSAGE_ID;
import static com.manager.gedu.api.DataTranferObjectConstants.API_MESSAGE_TIMESTAMP;

/**
 * @author Weverton Souza.
 * Created on 30/06/19
 */
public abstract class AbstractDataTransferObject implements IDataTransferObject {
    @ApiModelProperty(notes = API_MESSAGE_ID)
    protected String id;

    public String getId() {
        return id;
    }

    public IDataTransferObject setId(String id) {
        this.id = id;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || getClass() != o.getClass()) return false;
        IDataTransferObject that = (IDataTransferObject) o;
        return Objects.equals(id, that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
