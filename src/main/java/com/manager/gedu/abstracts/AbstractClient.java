package com.manager.gedu.abstracts;

import com.google.gson.Gson;
import com.manager.gedu.domain.address.ibge.County;
import com.manager.gedu.domain.address.ibge.UF;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestOperations;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;

/**
 * @author Weverton Souza.
 * Created on 27/06/19
 */
public abstract class AbstractClient {
    protected final Logger logger = LoggerFactory.getLogger(getClass());
    protected RestOperations restOperations;
    protected URL url;
    protected HttpURLConnection con;
    protected HttpEntity<?> entity;
    protected UriComponentsBuilder builder;
    protected HttpHeaders headers;

    @Value("${gedu.viacep-service-url}")
    protected String viaCepUrl;

    @Value("${gedu.restcountries-service-countries-url}")
    protected String restcountriesUrl;

    @Value("${gedu.ibge-service-localidades-url}")
    protected String IBGEServiceLocale;

    protected Gson gson = new Gson().newBuilder().create();

    public AbstractClient() {
        this.restOperations = new RestTemplate();
        this.headers = new HttpHeaders();
        this.headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
    }

    protected void connect(final String API_URL, final String TYPE_REQUEST) {
        url = null;
        con = null;
        try {
            URL url = new URL(API_URL);
            con = (HttpURLConnection) url.openConnection();
            con.setRequestProperty("Content-Type", "application/json");
            con.setUseCaches(false);
            con.setRequestMethod(TYPE_REQUEST);

            if (con.getResponseCode() != HttpStatus.OK.value()) {
                throw new RuntimeException("HTTP error code : " + con.getResponseCode());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected void closeConnect() {
        this.con.disconnect();
    }


    protected String call() {
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader((con.getInputStream())));
            StringBuilder sb = new StringBuilder();
            String line;

            while ((line = bufferedReader.readLine()) != null) { sb.append(line); }

            bufferedReader.close();
            return sb.toString();

        } catch (IOException e) {
            return null;
        } finally {
            this.closeConnect();
        }
    }

}
