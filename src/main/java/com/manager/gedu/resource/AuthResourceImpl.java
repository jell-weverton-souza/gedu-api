package com.manager.gedu.resource;

import com.manager.gedu.datatransferobject.CredentialsDTO;
import com.manager.gedu.datatransferobject.user.UserAccessDTO;
import com.manager.gedu.enums.GeduHttpStatus;
import com.manager.gedu.generic.GenericResponse;
import com.manager.gedu.service.UserAccessServiceImpl;
import io.swagger.annotations.Api;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@CrossOrigin
@Api(value = "Auth", tags = "Auth")
@RequestMapping("/")
public class AuthResourceImpl {
    private final UserAccessServiceImpl service;

    public AuthResourceImpl(final UserAccessServiceImpl service) {
        this.service = service;
    }

    @PostMapping(value = "/login")
    public GenericResponse<UserAccessDTO> loadUserAccess(@Valid @RequestBody final CredentialsDTO credentials) {
        return new GenericResponse<>(
                this.service.loadUserAccess(credentials),
                HttpStatus.OK.value(),
                HttpStatus.OK,
                GeduHttpStatus.OK.getMessage());
    }
}
