package com.manager.gedu.resource;

import com.manager.gedu.abstracts.AbstractResource;
import com.manager.gedu.datatransferobject.gradesubject.GradeDTO;
import com.manager.gedu.service.GradeServiceImpl;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.manager.gedu.api.GeduConstants.BASE_URL_STAFF_NAME;
import static com.manager.gedu.api.GeduConstants.BASE_URL_STAFF_TAG;

@RestController
@CrossOrigin
@Api(value = "Grade", tags = "Grade")
@RequestMapping("/grades")
public class GradeResourceImpl extends AbstractResource<GradeDTO, String> {
    @Autowired
    public GradeResourceImpl(final GradeServiceImpl service) {
        super(service);
    }
}
