package com.manager.gedu.resource;

import com.manager.gedu.abstracts.AbstractResource;
import com.manager.gedu.datatransferobject.task.ScoreDTO;
import com.manager.gedu.service.ScoreServiceImpl;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.manager.gedu.api.GeduConstants.BASE_URL_STAFF_NAME;
import static com.manager.gedu.api.GeduConstants.BASE_URL_STAFF_TAG;

@RestController
@CrossOrigin
@Api(value = "Score", tags = "Score")
@RequestMapping("/scores")
public class ScoreResourceImpl extends AbstractResource<ScoreDTO, String> {
    @Autowired
    public ScoreResourceImpl(final ScoreServiceImpl service) {
        super(service);
    }
}
