package com.manager.gedu.resource;

import com.manager.gedu.abstracts.AbstractResource;
import com.manager.gedu.datatransferobject.address.AddressDTO;
import com.manager.gedu.enums.GeduHttpStatus;
import com.manager.gedu.generic.GenericResponse;
import com.manager.gedu.service.AddressServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import static com.manager.gedu.api.GeduConstants.*;

@RestController
@CrossOrigin
@Api(value = BASE_URL_ADDRESS_NAME, tags = BASE_URL_ADDRESS_TAG)
@RequestMapping(BASE_URL_ADDRESS_NAME)
public class AddressResourceImpl extends AbstractResource<AddressDTO, String> {
    private final AddressServiceImpl service;

    @Autowired
    public AddressResourceImpl(final AddressServiceImpl service) {
        super(service);
        this.service = service;
    }

    @GetMapping(value = "/states")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = RESOURCE_OPERATION_FIND_BY_ID, response=GenericResponse.class)
    @ApiResponses( value = {
            @ApiResponse(code=200, message=HTTP_MESSAGE_200, response= GenericResponse.class),
            @ApiResponse(code=401, message=HTTP_MESSAGE_401, response=GenericResponse.class),
            @ApiResponse(code=403, message=HTTP_MESSAGE_403, response=GenericResponse.class),
            @ApiResponse(code=404, message=HTTP_MESSAGE_404, response=GenericResponse.class)
    })
    public GenericResponse fetchAllStates() {
        return new GenericResponse<>(
                this.service.fetchAllStates(),
                HttpStatus.OK.value(),
                HttpStatus.OK,
                GeduHttpStatus.OK.getMessage());
    }

    @GetMapping(value = "states/{state-code}/counties")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = RESOURCE_OPERATION_FIND_BY_ID, response=GenericResponse.class)
    @ApiResponses( value = {
            @ApiResponse(code=200, message=HTTP_MESSAGE_200, response= GenericResponse.class),
            @ApiResponse(code=401, message=HTTP_MESSAGE_401, response=GenericResponse.class),
            @ApiResponse(code=403, message=HTTP_MESSAGE_403, response=GenericResponse.class),
            @ApiResponse(code=404, message=HTTP_MESSAGE_404, response=GenericResponse.class)
    })
    public GenericResponse findCounty(@PathVariable("state-code") final Long stateCode) {
        return new GenericResponse<>(
                this.service.findAllCitiesByState(stateCode),
                HttpStatus.OK.value(),
                HttpStatus.OK,
                GeduHttpStatus.OK.getMessage());
    }

    @GetMapping(value = "zip-code/{zip-code}")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = RESOURCE_OPERATION_FIND_BY_ID, response=GenericResponse.class)
    @ApiResponses( value = {
            @ApiResponse(code=200, message=HTTP_MESSAGE_200, response= GenericResponse.class),
            @ApiResponse(code=401, message=HTTP_MESSAGE_401, response=GenericResponse.class),
            @ApiResponse(code=403, message=HTTP_MESSAGE_403, response=GenericResponse.class),
            @ApiResponse(code=404, message=HTTP_MESSAGE_404, response=GenericResponse.class)
    })
    public GenericResponse findAddressByZipCode(@PathVariable("zip-code") final String zipCode) {
        return new GenericResponse<>(
                this.service.findAddressByZipCode(zipCode),
                HttpStatus.OK.value(),
                HttpStatus.OK,
                GeduHttpStatus.OK.getMessage());
    }
}
