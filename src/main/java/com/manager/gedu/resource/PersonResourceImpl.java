package com.manager.gedu.resource;

import com.manager.gedu.abstracts.AbstractResource;
import com.manager.gedu.datatransferobject.user.PersonDTO;
import com.manager.gedu.service.PersonServiceImpl;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.manager.gedu.api.GeduConstants.*;

@RestController
@CrossOrigin
@Api(value = BASE_URL_PERSON_NAME, tags = BASE_URL_PERSON_TAG)
@RequestMapping(BASE_URL_PERSON)
public class PersonResourceImpl extends AbstractResource<PersonDTO, String> {
    @Autowired
    public PersonResourceImpl(final PersonServiceImpl service) {
        super(service);
    }
}
