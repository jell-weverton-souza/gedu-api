package com.manager.gedu.resource;

import com.manager.gedu.abstracts.AbstractResource;
import com.manager.gedu.datatransferobject.student.StudentDTO;
import com.manager.gedu.service.StudentServiceImpl;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.manager.gedu.api.GeduConstants.BASE_URL_STAFF_NAME;
import static com.manager.gedu.api.GeduConstants.BASE_URL_STAFF_TAG;

@RestController
@CrossOrigin
@Api(value = "Student", tags = "Student")
@RequestMapping("/students")
public class StudentResourceImpl extends AbstractResource<StudentDTO, String> {
    @Autowired
    public StudentResourceImpl(final StudentServiceImpl service) {
        super(service);
    }
}
