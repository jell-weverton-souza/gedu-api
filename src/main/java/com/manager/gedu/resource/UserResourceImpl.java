package com.manager.gedu.resource;

import com.manager.gedu.abstracts.AbstractResource;
import com.manager.gedu.datatransferobject.user.UserDTO;
import com.manager.gedu.service.UserServiceImpl;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.manager.gedu.api.GeduConstants.BASE_URL_USER_NAME;
import static com.manager.gedu.api.GeduConstants.BASE_URL_USER_TAG;

@RestController
@CrossOrigin
@Api(value = BASE_URL_USER_NAME, tags = BASE_URL_USER_TAG)
@RequestMapping(BASE_URL_USER_NAME)
public class UserResourceImpl extends AbstractResource<UserDTO, String> {
    @Autowired
    public UserResourceImpl(final UserServiceImpl service) {
        super(service);
    }
}
