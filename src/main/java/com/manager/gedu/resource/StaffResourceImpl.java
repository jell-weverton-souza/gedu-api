package com.manager.gedu.resource;

import com.manager.gedu.abstracts.AbstractResource;
import com.manager.gedu.datatransferobject.staff.StaffDTO;
import com.manager.gedu.service.StaffServiceImpl;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.manager.gedu.api.GeduConstants.BASE_URL_STAFF_NAME;
import static com.manager.gedu.api.GeduConstants.BASE_URL_STAFF_TAG;

@RestController
@CrossOrigin
@Api(value = BASE_URL_STAFF_NAME, tags = BASE_URL_STAFF_TAG)
@RequestMapping(BASE_URL_STAFF_NAME)
public class StaffResourceImpl extends AbstractResource<StaffDTO, String> {
    @Autowired
    public StaffResourceImpl(final StaffServiceImpl service) {
        super(service);
    }
}
