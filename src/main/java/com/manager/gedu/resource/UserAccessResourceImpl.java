package com.manager.gedu.resource;

import com.manager.gedu.abstracts.AbstractResource;
import com.manager.gedu.datatransferobject.user.UserAccessDTO;
import com.manager.gedu.service.UserAccessServiceImpl;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.manager.gedu.api.GeduConstants.BASE_URL_USER_ACCESS_NAME;
import static com.manager.gedu.api.GeduConstants.BASE_URL_USER_ACCESS_TAG;

@RestController
@CrossOrigin
@Api(value = BASE_URL_USER_ACCESS_NAME, tags = BASE_URL_USER_ACCESS_TAG)
@RequestMapping(BASE_URL_USER_ACCESS_NAME)
public class UserAccessResourceImpl extends AbstractResource<UserAccessDTO, String> {
    private final UserAccessServiceImpl service;

    @Autowired
    public UserAccessResourceImpl(final UserAccessServiceImpl service) {
        super(service);
        this.service = service;
    }

}
