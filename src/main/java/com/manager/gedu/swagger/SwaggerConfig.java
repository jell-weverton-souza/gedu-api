package com.manager.gedu.swagger;

import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author Weverton Souza.
 * Created on 04/07/19
 */
@EnableSwagger2
@EnableWebMvc
public class SwaggerConfig extends BaseSwaggerConfig {
    public SwaggerConfig() {
        super();
    }
}
