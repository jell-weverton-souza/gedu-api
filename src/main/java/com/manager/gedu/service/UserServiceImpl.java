package com.manager.gedu.service;

import com.manager.gedu.abstracts.AbstractService;
import com.manager.gedu.datatransferobject.user.UserDTO;
import com.manager.gedu.mapper.IUserMapper;
import com.manager.gedu.repository.IUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl extends AbstractService<UserDTO, String> {
    @Autowired
    public UserServiceImpl(IUserRepository repository, IUserMapper mapper) {
        super(repository, mapper);
    }
}
