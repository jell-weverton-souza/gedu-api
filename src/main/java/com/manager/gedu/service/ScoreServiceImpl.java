package com.manager.gedu.service;

import com.manager.gedu.abstracts.AbstractService;
import com.manager.gedu.datatransferobject.task.ScoreDTO;
import com.manager.gedu.mapper.IScoreMapper;
import com.manager.gedu.repository.IScoreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ScoreServiceImpl extends AbstractService<ScoreDTO, String> {
    @Autowired
    public ScoreServiceImpl(IScoreRepository repository, IScoreMapper mapper) {
        super(repository, mapper);
    }
}
