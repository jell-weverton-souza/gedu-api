package com.manager.gedu.service;

import com.manager.gedu.abstracts.AbstractService;
import com.manager.gedu.datatransferobject.staff.StaffDTO;
import com.manager.gedu.mapper.IStaffMapper;
import com.manager.gedu.repository.IUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StaffServiceImpl extends AbstractService<StaffDTO, String> {
    @Autowired
    public StaffServiceImpl(IUserRepository repository, IStaffMapper mapper) {
        super(repository, mapper);
    }
}
