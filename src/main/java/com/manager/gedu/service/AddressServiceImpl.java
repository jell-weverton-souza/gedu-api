package com.manager.gedu.service;

import com.manager.gedu.abstracts.AbstractService;
import com.manager.gedu.datatransferobject.address.AddressDTO;
import com.manager.gedu.domain.address.ViaCep;
import com.manager.gedu.domain.address.ibge.County;
import com.manager.gedu.domain.address.ibge.UF;
import com.manager.gedu.infrastructure.client.IBGE;
import com.manager.gedu.infrastructure.client.ViaCepClient;
import com.manager.gedu.mapper.IAddressMapper;
import com.manager.gedu.repository.IAddressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AddressServiceImpl extends AbstractService<AddressDTO, String> {
    private final IBGE IBGEClient;
    private final ViaCepClient viaCepClient;

    @Autowired
    public AddressServiceImpl(final IBGE IBGEClient,
                              final ViaCepClient viaCepClient, IAddressRepository repository, IAddressMapper mapper) {
        super(repository, mapper);
        this.IBGEClient = IBGEClient;
        this.viaCepClient = viaCepClient;
    }

    public List<County> findAllCitiesByState(final Long countyCod) {
        return this.IBGEClient.findAllCitiesByState(countyCod);
    }

    public List<UF> fetchAllStates() {
        return this.IBGEClient.fetchAllStates();
    }

    public ViaCep findAddressByZipCode(final String zipCode) {
        return this.viaCepClient.get(zipCode);
    }
}
