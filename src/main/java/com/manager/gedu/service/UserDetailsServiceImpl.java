package com.manager.gedu.service;

import com.manager.gedu.domain.user.UserAccess;
import com.manager.gedu.repository.IUserAccessRepository;
import com.manager.gedu.security.configuration.UserDetailImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * @author Weverton Souza.
 * Created on 01/07/19
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    private IUserAccessRepository repository;

    @Autowired
    UserDetailsServiceImpl(final IUserAccessRepository repository) {
        this.repository = repository;
    }

    @Override
    public UserDetailImpl loadUserByUsername(final String email) throws UsernameNotFoundException {
        Optional<UserAccess> optionalUser = this.repository.findByEmail(email);
        UserDetailImpl userDetail;

        if (!optionalUser.isPresent()) {
            throw new UsernameNotFoundException(email);
        }

        return new UserDetailImpl()
                .setId(optionalUser.get().getId())
                .setEmail(optionalUser.get().getEmail())
                .setPassword(optionalUser.get().getPassword());
    }
}
