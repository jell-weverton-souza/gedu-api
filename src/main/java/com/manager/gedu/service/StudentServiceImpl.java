package com.manager.gedu.service;

import com.manager.gedu.abstracts.AbstractService;
import com.manager.gedu.datatransferobject.student.StudentDTO;
import com.manager.gedu.mapper.IStudentMapper;
import com.manager.gedu.repository.IStudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StudentServiceImpl extends AbstractService<StudentDTO, String> {
    @Autowired
    public StudentServiceImpl(IStudentRepository repository, IStudentMapper mapper) {
        super(repository, mapper);
    }
}
