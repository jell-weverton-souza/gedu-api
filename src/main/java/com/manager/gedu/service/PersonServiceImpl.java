package com.manager.gedu.service;

import com.manager.gedu.abstracts.AbstractService;
import com.manager.gedu.datatransferobject.user.PersonDTO;
import com.manager.gedu.mapper.IPersonMapper;
import com.manager.gedu.repository.IPersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PersonServiceImpl extends AbstractService<PersonDTO, String> {
    @Autowired
    public PersonServiceImpl(IPersonRepository repository, IPersonMapper mapper) {
        super(repository, mapper);
    }
}
