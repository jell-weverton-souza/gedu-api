package com.manager.gedu.service;

import com.manager.gedu.abstracts.AbstractService;
import com.manager.gedu.datatransferobject.gradesubject.GradeDTO;
import com.manager.gedu.mapper.IGradeMapper;
import com.manager.gedu.repository.IGradeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GradeServiceImpl extends AbstractService<GradeDTO, String> {
    @Autowired
    public GradeServiceImpl(IGradeRepository repository, IGradeMapper mapper) {
        super(repository, mapper);
    }
}
