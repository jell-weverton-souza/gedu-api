package com.manager.gedu.infrastructure.template;

import com.manager.gedu.infrastructure.client.IBGE;
import com.manager.gedu.infrastructure.client.RestCountries;
import com.manager.gedu.infrastructure.client.ViaCepClient;
import com.manager.gedu.interfaces.IRestTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Weverton Souza.
 * Created on 27/06/19
 */
@Configuration
public class RestTemplate implements IRestTemplate {

    @Bean
    @Override
    public ViaCepClient getViaCepClient() {
        return new ViaCepClient();
    }

    @Bean
    @Override
    public RestCountries getRestCountriesClient() {
        return new RestCountries();
    }

    @Bean
    @Override
    public IBGE getRestIBGEClient() {
        return new IBGE();
    }
}
