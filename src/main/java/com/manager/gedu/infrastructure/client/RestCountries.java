package com.manager.gedu.infrastructure.client;

import com.manager.gedu.abstracts.AbstractClient;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.util.UriComponentsBuilder;


/**
 * @author Weverton Souza.
 * Created on 01/07/19
 */
public class RestCountries extends AbstractClient {

    public RestCountries() {}

    public Country get() {
        this.logger.info("Get all countries");
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(restcountriesUrl);
        HttpEntity<?> entity = new HttpEntity<>(headers);

        this.logger.info(builder.toUriString());
        String body = restOperations
                .exchange(builder.toUriString(), HttpMethod.GET, entity, String.class).getBody();

        this.logger.info("\n" + body);
        return this.gson.fromJson(body, Country.class);
    }
}
