package com.manager.gedu.infrastructure.client;

import com.manager.gedu.abstracts.AbstractClient;
import com.manager.gedu.domain.address.ViaCep;
import org.springframework.stereotype.Component;

/**
 * @author Weverton Souza.
 * Created on 27/06/19
 */
@Component
public class ViaCepClient extends AbstractClient {

    public ViaCepClient() { }

    public ViaCep get(final String zipCode) {
        this.connect(String.format(this.viaCepUrl, zipCode), "GET");
        return this.gson.fromJson(this.call(), ViaCep.class);
    }
}
