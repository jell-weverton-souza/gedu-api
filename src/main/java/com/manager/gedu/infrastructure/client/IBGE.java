package com.manager.gedu.infrastructure.client;

import com.google.gson.Gson;
import com.manager.gedu.abstracts.AbstractClient;
import com.manager.gedu.domain.address.ibge.County;
import com.manager.gedu.domain.address.ibge.Region;
import com.manager.gedu.domain.address.ibge.UF;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.web.util.UriComponentsBuilder;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;

public class IBGE extends AbstractClient {
    private String URL_WS = "http://servicodados.ibge.gov.br/api/v1/localidades/estados";

    public IBGE() { }

    public Region findRegion(final String regionCod) {
        this.builder = UriComponentsBuilder.fromHttpUrl(IBGEServiceLocale + "/localidades/" + regionCod);
        this.entity = new HttpEntity<>(headers);

        String body = this.restOperations
                .exchange(builder.toUriString(), HttpMethod.GET, entity, String.class)
                .getBody();

        return this.gson.fromJson(body, Region.class);
    }

    public List<Region> findAllRegion() {
        this.builder = UriComponentsBuilder
                .fromHttpUrl(IBGEServiceLocale + "/localidades/" + "/1/2/3/4/5");
        this.entity = new HttpEntity<>(headers);

        String body = this.restOperations
                .exchange(builder.toUriString(), HttpMethod.GET, entity, String.class)
                .getBody();

        return this.gson.fromJson(body, (Type) Region[].class);
    }

    public List<County> findAllCitiesByState(final Long countyCod) {
        String url = this.IBGEServiceLocale+"/localidades/estados/"+countyCod+"/municipios";
        this.connect(this.IBGEServiceLocale+"/localidades/estados/"+countyCod+"/municipios", "GET");
        String page = this.call();
        return Arrays.asList(new Gson().fromJson(page, County[].class));
    }

    public List<UF> fetchAllStates() {
        this.connect(this.IBGEServiceLocale + "/localidades/estados", "GET");
        return Arrays.asList(new Gson().fromJson(this.call(), UF[].class));
    }

}
