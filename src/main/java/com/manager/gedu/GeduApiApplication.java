package com.manager.gedu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.config.EnableMongoAuditing;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@EnableMongoAuditing
@SpringBootApplication
public class GeduApiApplication {
	public static void main(String[] args) {
		SpringApplication.run(GeduApiApplication.class, args);
	}
}
