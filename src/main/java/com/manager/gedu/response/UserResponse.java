package com.manager.gedu.response;

import com.manager.gedu.domain.user.User;
import com.manager.gedu.enums.GeduHttpStatus;
import com.manager.gedu.generic.GenericResponse;
import org.springframework.http.HttpStatus;

/**
 * @author Weverton Souza.
 * Created on 04/07/19
 */
public class UserResponse extends GenericResponse<User> {
    public UserResponse() { }

    public UserResponse(final User content, final Integer code, final GeduHttpStatus status,
                        final String description) {
        super(content, code, HttpStatus.BAD_REQUEST, description);
    }
}
