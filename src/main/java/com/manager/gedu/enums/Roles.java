package com.manager.gedu.enums;

/**
 * @author Weverton Souza.
 * Created on 30/06/19
 */
public enum Roles {
    ADM_LEV1("ADM_LEV1", "Administrative Access; Level 01."),
    STD_LEV1("STD_LEV1","Student Access; Level 01."),
    STF_LEV1("STF_LEV1","Staff Access; Level 01."),
    STF_LEV2("STF_LEV2","Staff Access; Level 02.");

    private String name;
    private String description;

    Roles(final String name, final String description) {
        this.name = name;
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public String getName() {
        return name;
    }
}
