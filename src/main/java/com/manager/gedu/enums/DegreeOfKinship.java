package com.manager.gedu.enums;

public enum DegreeOfKinship {
    MOTHER("Administrative Access; Level 01."),
    HUSBAND("Student Access; Level 01."),
    SON("Staff Access; Level 01."),
    BROTHER("Staff Access; Level 02."),
    SISTER("Staff Access; Level 03.");

    private String description;

    DegreeOfKinship(final String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
