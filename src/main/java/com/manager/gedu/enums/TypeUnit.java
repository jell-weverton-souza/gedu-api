package com.manager.gedu.enums;

/**
 * @author Weverton Souza.
 * Created on 30/06/19
 */
public enum TypeUnit {
    ELEMENTARY_SCHOOL(0,"Ensino Infantil"),
    JUNIOR_HIGH_SCHOOL(1, "Ensino Fundamental"),
    HIGH_SCHOOL(2, "Ensino Médio"),
    HIGHER_EDUCATION(3, "Ensino Superior");

    private String description;
    private int index;

    TypeUnit(final int index, final String description) {
        this.index = index;
        this.description = description;
    }

    public int getIndex() {
        return this.index;
    }

    public String getDescription() {
        return this.description;
    }
}
