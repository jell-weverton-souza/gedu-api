package com.manager.gedu.api;

/**
 * @author Weverton Souza.
 * Created on 28/07/2019
 */
public class DataTranferObjectConstants {

    /* ********** REST CONSTANTS ********** */
    private static final String PROPERTY = "Property: ";
    private static final String ROLE = "Role: ";
    private static final String CONDITION = "Condition: ";
    public static final String API_MESSAGE_ID = PROPERTY + "Identidicador\n" +
            ROLE + "O banco de dados gera o id automaticamente.";
    public static final String API_MESSAGE_CREATE_BY = PROPERTY + "Criado por\n" +
            ROLE + "O banco de dados gera o createBy automaticamente.";
    public static final String API_MESSAGE_UPDATE_BY = PROPERTY + "Atualizado por\n" +
            ROLE + "O banco de dados gera o updateBy automaticamente.";
    public static final String API_MESSAGE_TIMESTAMP = PROPERTY + "Data de criação\n" +
            ROLE + "O banco de dados gera o timestamp automaticamente.";

    /* ********** Address ********** */
    public static final String ADDRESS_MESSAGE_LATITUDE = PROPERTY + "Latitude\n" +
            ROLE + "Somente aceita os seguintes caracteres: (0123456789 . -). " +
            "Sendo que o sinal de subtração “-” somente pode vir na posição inicial. " +
            "Deverá ser um número com no máximo 20 caracteres, onde a parte decimal poderá existir ou não. " +
            "Deve ser menor ou igual a 5.272222 e maior ou igual a -33.750833.\n";
    public static final String ADDRESS_MESSAGE_LONGITUDE = PROPERTY + "Longitude\n" +
            ROLE + "Somente aceita os seguintes caracteres: (0123456789 . -). Sendo que o sinal de subtração “-” " +
            "somente pode vir na posição inicial. Deverá ser um número com no máximo 20 caracteres, onde a parte decimal" +
            " poderá existir ou não. Deve ser menor ou igual a -32.411280 e maior ou igual a -73.992222.";
    public static final String ADDRESS_MESSAGE_ZIPCODE = PROPERTY + "CEP\n" +
            ROLE + "Apenas números deverão ser informados. Os 8 dígitos devem ser informados. " +
            "Não pode ser repetição de um único algarismo.";
    public static final String ADDRESS_MESSAGE_ADDRESS = PROPERTY + "Endereço\n" +
            ROLE + "Somente aceita os seguintes caracteres: (ABCDEFGHIJKLMNOPQRSTUVWXYZ 0123456789 ªº – / . ,).";
    public static final String ADDRESS_MESSAGE_ADDRESSNUMBER = PROPERTY + "Número de endereço\n" +
            ROLE + "Somente aceita os seguintes caracteres: (ABCDEFGHIJKLMNOPQRSTUVWXYZ 0123456789 ªº – / . ,).";
    public static final String ADDRESS_MESSAGE_ADDRESSCOMPLEMENT = PROPERTY + "Complemento\n" +
            ROLE + "Somente aceita os seguintes caracteres: (ABCDEFGHIJKLMNOPQRSTUVWXYZ 0123456789 ªº – / . ,).";
    public static final String ADDRESS_MESSAGE_NEIGHBORHOOD =
            PROPERTY + "Bairro.\n" +
                    ROLE +"Somente aceita os seguintes caracteres: (ABCDEFGHIJKLMNOPQRSTUVWXYZ 0123456789 ªº – / . ,).";
    public static final String ADDRESS_MESSAGE_STATE = PROPERTY + "Estado\n" +
            ROLE + "Deverá ser preenchido com o código do estado, de acordo com a “Tabela de UF”.";
    public static final String ADDRESS_MESSAGE_CITY = PROPERTY + "Cidade\n" +
            "Deverá ser preenchido com o código do município, de acordo com a “Tabela de Município”.";
    public static final String ADDRESS_MESSAGE_DISTRICT = PROPERTY + "Distrito\n" +
            "Deverá ser preenchido com o código do distrito, de acordo com a “Tabela de Distritos”.";

    /* ********** Unit ********** */
    public static final String UNIT_MESSAGE_RECORD_TYPE = PROPERTY +"Tipo de registro\n" + ROLE + "Valor válido: 00.\n"
            + CONDITION + "Deverá ser preenchido com o valor 00.";
    public static final String UNIT_MESSAGE_UNIT_CODE = PROPERTY + "Código de escola – Inep\n" +
            ROLE + "Apenas números deverão ser informados. Os 8 dígitos devem ser informados. " +
            "Não pode ser repetição de um único algarismo.";
    public static final String UNIT_MESSAGE_OPERATING_SITUATION = PROPERTY + "Situação de funcionamento\n" +
            ROLE + "Somente aceita os seguintes caracteres: (ABCDEFGHIJKLMNOPQRSTUVWXYZ 0123456789 ªº – / . ,).";
    public static final String UNIT_MESSAGE_ACADEMIC_YEAR = PROPERTY + "Ano Letivo\n" +
            ROLE + "O campo é obrigatório para as escolas em atividade. O formato é dd/mm/aaaa. " +
            "O valor informado deve corresponder a uma data válida.";
    public static final String UNIT_MESSAGE_REGIONAL_CODE = PROPERTY + "Código do órgão regional de ensino (*)\n" +
            ROLE + "Somente são aceitos códigos dos órgãos regionais constantes da “Tabela de Órgão Regional de Ensino. " +
            "O campo é obrigatório para os estados que possuem essas subdivisões, para os demais o" +
            " campo não deverá ter informação." +
            CONDITION + "O valor informado deverá estar entre os valores da “Tabela de Órgão Regional " +
            "de Ensino” possíveis para o estado informado.";
    public static final String UNIT_MESSAGE_ADMINISTRATIVE_DEPENDENCY =
            PROPERTY + "Dependência administrativa.\n" +
                    ROLE +"Somente uma opção poderá ser informada. Valores permitidos:\n" +
                    "1 – federal\n2 – estadual\n3 – municipal\n4 – privada" +
                    CONDITION + "Apenas usuários com perfil Inep ou Estado podem alterar esse dado.";
    public static final String UNIT_MESSAGE_SCHOOL_ZONE = PROPERTY + "Localização/Zona da escola\n" +
            ROLE + "Somente uma opção poderá ser informada.\n1 – urbana\n2 – rural" +
            CONDITION + "Apenas usuários com perfil Inep ou Estado podem alterar esse dado.";
    public static final String UNIT_MESSAGE_PRIVATE_SCHOOL_CATEGORY = PROPERTY + "Categoria da escola privada\n" +
            "O campo é obrigatório para as escolas privadas em atividade. Somente uma opção poderá ser informada.\n" +
            "Valores permitidos:\n1 – particular\n2 – comunitária\n3 – confessional\n4 – filantrópica" +
            CONDITION + "O Campo 3 deve ser igual a 1 e o campo 24 deve ser igual a 4, ambos do registro 00.";
    public static final String UNIT_MESSAGE_AGREED_WITH_THE_POWER_PUBLIC = PROPERTY + "Conveniada com o poder público\n" +
            "Apenas as escolas privadas em atividade podem informar este campo.\n" +
            "Somente uma opção poderá ser informada. Valores permitidos:\n" +
            "1 – estadual\n2 – municipal\n3 – estadual e municipal.\n"+
            CONDITION + "O Campo 3 deve ser igual a 1 e o campo 24 deve ser igual a 4, ambos do registro 00.";
    public static final String UNIT_MESSAGE_TAX_IDENTIFICATION_NUMBER = PROPERTY + "CNPJ\nCNPJ da escola privada"+
            CONDITION + "O Campo 3 deve ser igual a 1, o campo 24 deve ser igual a 4 e pelo menos um dos campos de " +
            "28 a 32 deve ter o valor 1; todos do registro 00..";
    public static final String UNIT_MESSAGE_ADDRESS = PROPERTY + "Endereço\nEndereço da unidade.";
    public static final String UNIT_MESSAGE_CONTACT_DETAILS = PROPERTY + "Dados de contato";
    public static final String UNIT_MESSAGE_UNIT_MAINTAINER = PROPERTY + "Mantenedora da escola privada";

    /* ********** UnitMaintainer ********** */
    public static final String UNIT_MAINTAINER = "Mantenedora da escola privada";
    public static final String UNIT_MAINTAINER_TYPE = "Mantenedora da escola privada\n" +
            ROLE + "Obrigatório para escola privada em atividade. Valores permitidos:\n0 – Não\n1 – Sim" +
            "Mais de um campo, do 28 ao 32, pode conter valor igual a 1." +
            CONDITION + "O Campo 3 deve ser igual a 1, o campo 24 deve ser igual a 4 e pelo menos um dos campos de " +
            "28 a 32 deve ter o valor 1; todos do registro 00.";
    public static final String UNIT_MAINTAINER_TAX_IDENTIFICATION_NUMBER = PROPERTY + "CNPJ da escola privada\n"+
            ROLE + "O CNPJ informado deve ser válido. Apenas números são aceitos.\n" +
            CONDITION + "O Campo 3 deve ser igual a 1, o campo 24 deve ser igual a 4 e pelo menos um dos campos de" +
            " 28 a 32 deve ter o valor 1; todos do registro 00.";
    public static final String UNIT_MAINTAINER_REGULATION = PROPERTY + "Regulamentação/Autorização no conselho ou órgão" +
            " municipal, estadual ou federal de educação.\nValores permitidos:\n0 – Não\n1 – Sim\n2 – Em tramitação" +
            ROLE + "O campo é obrigatório para as escolas em atividade." +
            CONDITION + "O Campo 3 do registro 00 deve ser igual a 1.";

    /* ********** InfrastructureUnit ********** */
    public static final String INFRASTRUCTURE_UNIT = "Infraestrutura da unidade";
    public static final String INFRASTRUCTURE_UNIT_DESCRIPTION = "Details about the infrastructure unit";
    public static final String INFRASTRUCTURE_UNIT_LOCATION_OF_OPERATION =
            PROPERTY + "10.07-15 - Local de funcionamento da escola\n" +
                    ROLE + "Campo Obrigatório.\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String INFRASTRUCTURE_UNIT_BUILDING_TYPE_OCCUPANCY =
            PROPERTY + "10.16 - Forma prédio de ocupação do prédio\n" +
                    ROLE + "Campo obrigatório;\nValores permitidos:\n1 - Próprio\n2 - Aluguel\n3 - Cedidoa\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String INFRASTRUCTURE_UNIT_SHARED_BUILDING_WITH_ANOTHER_SCHOOL =
            PROPERTY + "10.17 - Prédio compartilhado com outra escola.\n" +
                    ROLE + "Campo obrigatório.\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String INFRASTRUCTURE_UNIT_UNITS =
            PROPERTY + "10.18-23 - Código da escola com a qual compartilha.\n" +
                    ROLE + "Campo obrigatório.\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1, e, os campos infrastructureUnitType" +
                    " (10.07-15) e isSharedBuildingwithAnotherSchool (10.17). O código não ser o mesmo da escola." +
                    " As validações de situação de funcionamento e existência do código do Inep da escola serão" +
                    " feitas no momento do processamento do arquivo.";
    public static final String INFRASTRUCTURE_UNIT_HAS_FILTERED_WATER_SUPPLY =
            PROPERTY + "10.17 - Prédio compartilhado com outra escola.\n" +
                    ROLE + "Campo obrigatório. Valores permitidos: 1 – Não filtrada (false)\n2 – Filtrada (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String INFRASTRUCTURE_UNIT_TOTAL_STAFF =
            PROPERTY + "10.44 - Sala de professores.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String INFRASTRUCTURE_UNIT_UNIT_FEEDING_FOR_STUDENTS =
            PROPERTY + "10.92 - Alimentação escolar para os alunos.\n" +
                    ROLE + "Campo obrigatório. 0 – Não oferece (false)\n1 – Oferece (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1, e, se o campo 00.24 for 1, 2 ou 3" +
                    " este campo deverá, obrigatoriamente, ser 1.";
    public static final String INFRASTRUCTURE_UNIT_WATER_SUPPLY_TYPES =
            PROPERTY + "10.25-29 - Abastecimento de água.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String INFRASTRUCTURE_UNIT_TYPE_ELECTRIC_POWER_SUPPLIES =
            PROPERTY + "10.30-33 - Abastecimento de energia elétrica.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String INFRASTRUCTURE_UNIT_SANITARY_SEWER_TYPES =
            PROPERTY + "10.34-36 - Esgoto sanitário.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String INFRASTRUCTURE_UNIT_WASTE_DESTINATION_TYPES =
            PROPERTY + "10.37-42 - Destinação do lixo.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String INFRASTRUCTURE_UNIT_DEPENDENCIES_OF_UNITS =
            PROPERTY + "10.43-72 - Dependências existentes na escola.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String INFRASTRUCTURE_UNIT_UNIT_EQUIPMENTS =
            PROPERTY + "10.75-86 - Equipamentos existentes na escola.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String INFRASTRUCTURE_UNIT_EDUCATIONAL_DATA =
            PROPERTY + "10.93-94 - Dados Educacionais.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não oferece\n1 – Não exclusivamente\n2 – Exclusivamente\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1. Se informado o valor 2, o campo 94" +
                    " deve ser igual a 0 e os campos de 95 a 118 do registro 10 devem ser nulos. Se informado o" +
                    " valor 1, o campo 94 do registro 10 somente poderá conter os valores 0 ou 1.";

    /* ********** WaterSupplyType ********** */
    public static final String WATER_SUPPLY_TYPE = "Water Supply Type";
    public static final String WATER_SUPPLY_TYPE_DESCRIPTION = "Details about the infrastructure unit";
    public static final String WATER_SUPPLY_TYPE_HAS_PUBLIC_NETWORK =
            PROPERTY + "10.25 - Rede pública.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String WATER_SUPPLY_TYPE_HAS_ARTESIAN_WELL =
            PROPERTY + "10.26 - Poço artesiano.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String WATER_SUPPLY_TYPE_HAS_WELL =
            PROPERTY + "10.27 - Cacimba/Cisterna/Poço.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String WATER_SUPPLY_TYPE_HAS_RIVER_CREEKS_STREAM_IGARAPE =
            PROPERTY + "10.28 - Fonte/Rio/Igarapé/Riacho/Córrego.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";

    /* ********** ElectricPowerSupplyType ********** */
    public static final String ELECTRIC_POWER_SUPPLY_TYPE = "Electric Power Supply Type";
    public static final String ELECTRIC_POWER_SUPPLY_TYPE_DESCRIPTION = "Details about the electric power supply type";
    public static final String ELECTRIC_POWER_SUPPLY_TYPE_PUBLIC_NETWORK =
            PROPERTY + "10.30 - Rede pública.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String ELECTRIC_POWER_SUPPLY_TYPE_POWER_GENERATOR =
            PROPERTY + "10.31 - Gerador.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String ELECTRIC_POWER_SUPPLY_TYPE_OTHERS_TYPES_POWER_GENERATOR =
            PROPERTY + "10.32 - Outros (energia alternativa). Ex: eólica, solar, etc.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";

    /* ********** SanitarySewerType ********** */
    public static final String SANITARY_SEWER_TYPE = "Sanitary Sewer Type";
    public static final String SANITARY_SEWER_TYPE_DESCRIPTION = "Details about the electric power supply type";
    public static final String SANITARY_SEWER_TYPE_PUBLIC_NETWORK =
            PROPERTY + "10.34 - Rede pública.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String SANITARY_SEWER_TYPE_SEPTIC_TANK =
            PROPERTY + "10.35 - Fossa.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";

    /* ********** WasteDestinationType ********** */
    public static final String WASTE_DESTINATION_TYPE = "Waste Destination Type";
    public static final String WASTE_DESTINATION_TYPE_DESCRIPTION = "Details about the infrastructure unit";
    public static final String WASTE_DESTINATION_TYPE_PERIODIC_COLLECTION=
            PROPERTY + "10.37 - Coleta periódica.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String WASTE_DESTINATION_TYPE_BURN_WASTE=
            PROPERTY + "10.38 - Queima.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String WASTE_DESTINATION_TYPE_DISPOSE_ANOTHER_AREA=
            PROPERTY + "10.39 - Joga em outra área.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String WASTE_DESTINATION_TYPE_WASTE_RECYCLING=
            PROPERTY + "10.40 - Recicla.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String WASTE_DESTINATION_TYPE_BURY_WASTE=
            PROPERTY + "10.41 - Enterra.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";

    /* ********** DependenciesOfSchoolDataTransfer ********** */
    public static final String DEPENDENCIES_OF_SCHOOL="Dependencies Of School";
    public static final String DEPENDENCIES_OF_SCHOOL_DESCRIPTION="Details about dependencies of school";
    public static final String DEPENDENCIES_OF_SCHOOL_HAS_SCHOOL_PRINCIPAL_OFFICE=
            PROPERTY + "10.43 - Sala de diretoria.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String DEPENDENCIES_OF_SCHOOL_HAS_STAFFROOM=
            PROPERTY + "10.44 - Sala de professores.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String DEPENDENCIES_OF_SCHOOL_HAS_SCHOOL_SECRETARIAT=
            PROPERTY + "10.45 - Sala de Secretaria.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String DEPENDENCIES_OF_SCHOOL_HAS_COMPUTERLAB=
            PROPERTY + "10.46 - Laboratório de informática.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String DEPENDENCIES_OF_SCHOOL_HAS_SCIENCELAB=
            PROPERTY + "10.47 - Laboratório de ciências.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String
            DEPENDENCIES_OF_SCHOOL_HAS_MULTI_FUNCTIONAL_RESOURCE_ROOM_FOR_SPECIALIZED_EDUCATIONAL_SERVICE=
            PROPERTY + "10.48 - Sala de recursos multifuncionais para atendimento educacional especializado - AEE.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String DEPENDENCIES_OF_SCHOOL_HAS_SPORTS_COURT_COVERED=
            PROPERTY + "10.49 - Quadra de esportes coberta.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String DEPENDENCIES_OF_SCHOOL_HAS_SPORTS_COURT_NOT_COVERED=
            PROPERTY + "10.50 - Quadra de esportes descoberta.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String DEPENDENCIES_OF_SCHOOL_HAS_KITCHEN=
            PROPERTY + "10.51 - Cozinha.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String DEPENDENCIES_OF_SCHOOL_HAS_LIBRARY=
            PROPERTY + "10.52 - Biblioteca.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String DEPENDENCIES_OF_SCHOOL_HAS_READING_ROOM=
            PROPERTY + "10.53 - Sala de Leitura.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String DEPENDENCIES_OF_SCHOOL_HAS_PLAYGROUND=
            PROPERTY + "10.54 - Parque infantil.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String DEPENDENCIES_OF_SCHOOL_HAS_NURSERY=
            PROPERTY + "10.55 - Berçário.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String DEPENDENCIES_OF_SCHOOL_HAS_BATHROOM_OUTSIDE_BUILDING=
            PROPERTY + "10.56 - Banheiro fora do prédio.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String DEPENDENCIES_OF_SCHOOL_HAS_BATHROOM_INSIDE_BUILDING=
            PROPERTY + "10.57 - Banheiro dentro do prédio.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String DEPENDENCIES_OF_SCHOOL_HAS_BATHROOM_SUITABLE_FOR_KINDERGARTEN=
            PROPERTY + "10.58 - Banheiro adequado à educação infantil.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String
            DEPENDENCIES_OF_SCHOOL_HAS_BATHROOM_SUITABLE_FOR_STUDENTS_WITH_DISABILITIES_OR_REDUCEDMOBILITY=
            PROPERTY + "10.59 - Banheiro adequado a alunos com deficiência ou mobilidade reduzida.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String
            DEPENDENCIES_OF_SCHOOL_HAS_DEPENDENCIES_AND_ROUTES_SUITABLE_FOR_STUDENTS_WITH_DISABILITIES_OR_REDUCEDMOBILITY=
            PROPERTY + "10.60 - Banheiro adequado a alunos com deficiência ou mobilidade reduzida.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String DEPENDENCIES_OF_SCHOOL_HAS_BATHROOM_WITH_SHOWER=
            PROPERTY + "10.61 - Banheiro com chuveiro.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String DEPENDENCIES_OF_SCHOOL_HAS_CAFETERIA=
            PROPERTY + "10.62 - Refeitório.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String DEPENDENCIES_OF_SCHOOL_HAS_PANTRY=
            PROPERTY + "10.63 - Despensa.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String DEPENDENCIES_OF_SCHOOL_HAS_WAREHOUSE=
            PROPERTY + "10.64 - Almoxarifado.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String DEPENDENCIES_OF_SCHOOL_HAS_AUDITORIUM=
            PROPERTY + "10.65 - Auditório.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String DEPENDENCIES_OF_SCHOOL_HAS_PATIO_COVERED=
            PROPERTY + "10.66 - Pátio coberto.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String DEPENDENCIES_OF_SCHOOL_HAS_PATIO_NOT_COVERED=
            PROPERTY + "10.67 - Pátio descoberto.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String DEPENDENCIES_OF_SCHOOL_HAS_STUDENT_ACCOMMODATION=
            PROPERTY + "10.68 - Alojamento de aluno.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String DEPENDENCIES_OF_SCHOOL_HAS_TEACHER_ACCOMMODATION=
            PROPERTY + "10.69 - Alojamento de professor.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String DEPENDENCIES_OF_SCHOOL_HAS_GREENAREA=
            PROPERTY + "10.70 - Área verde.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String DEPENDENCIES_OF_SCHOOL_HAS_LAUNDRY=
            PROPERTY + "10.71 - Lavanderia.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String DEPENDENCIES_OF_SCHOOL_NUMBER_OF_CLASSROOMS=
            PROPERTY + "10.73 - Número de salas de aula existentes na escola.\n" +
                    ROLE + "Campo obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String DEPENDENCIES_OF_SCHOOL_NUMBER_OF_ROOMS_USED_AS_CLASSROOM=
            PROPERTY + "10.74 - Número de salas utilizadas como sala de aula – Dentro e fora do prédio.\n" +
                    ROLE + "Campo obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";

    /* ********** UnitEquipmentDataTransfer ********** */
    public static final String UNIT_EQUIPMENT = "Unit Equipment";
    public static final String UNIT_EQUIPMENT_DESCRIPTION = "Details about unit equipment";
    public static final String UNIT_EQUIPMENT_HAS_TELEVISION_DEVICE=
            PROPERTY + "10.75 - Aparelho de Televisão.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String UNIT_EQUIPMENT_HAS_VCR=
            PROPERTY + "10.76 - Videocassete.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String UNIT_EQUIPMENT_HAS_DVD_DEVICE=
            PROPERTY + "10.77 - DVD.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String UNIT_EQUIPMENT_HAS_PARABOLIC_ANTENNA=
            PROPERTY + "10.78 - Antena parabólica.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String UNIT_EQUIPMENT_HAS_COPY_MACHINE=
            PROPERTY + "10.79 - Copiadora.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String UNIT_EQUIPMENT_HAS_OVERHEAD_PROJECTOR=
            PROPERTY + "10.80 - Retroprojetor.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String UNIT_EQUIPMENT_HAS_PRINTER=
            PROPERTY + "10.81 - Impressora.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String UNIT_EQUIPMENT_HAS_STEREO_SYSTEM=
            PROPERTY + "10.82 - Aparelho de som.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String UNIT_EQUIPMENT_HAS_DATA_SHOW=
            PROPERTY + "10.83 - Projetor Multimídia (Data show).\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String UNIT_EQUIPMENT_HAS_FAX=
            PROPERTY + "10.84 - Fax.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String UNIT_EQUIPMENT_HAS_CAMERA=
            PROPERTY + "10.85 - Máquina Fotográfica/Filmadora.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String UNIT_EQUIPMENT_HAS_COMPUTER=
            PROPERTY + "10.86 - Computadores.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String UNIT_EQUIPMENT_NUMBER_OF_COMPUTERS_OF_ADMINISTRATIVE_USE=
            PROPERTY + "10.87 - Quantidade de computadores de uso administrativo.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String UNIT_EQUIPMENT_NUMBER_OF_COMPUTERS_OF_STUDENT_USE=
            PROPERTY + "10.88 - Quantidade de computadores de uso dos alunos.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String UNIT_EQUIPMENT_HAS_INTERNET_ACCESS=
            PROPERTY + "10.89 - Acesso à Internet.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String UNIT_EQUIPMENT_HAS_BROADBAND=
            PROPERTY + "10.90 - Banda larga.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";

    /* ********** EducationalData ********** */
    public static final String EDUCATIONAL_DATA = "Educational Data";
    public static final String EDUCATIONAL_DATA_DESCRIPTION = "Details about educational data";
    public static final String EDUCATIONAL_DATA_SPECIALIZED_EDUCATIONAL_SERVICE=
            PROPERTY + "10.93 - Atendimento educacional especializado – AEE.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não oferece\n1 – Não exclusivamente(true)\n2 – Exclusivamente\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1. Se informado o valor 2, o campo" +
                    " 94 deve ser igual a 0 e os campos de 95 a 118 do registro 10 devem ser nulos. Se informado" +
                    " o valor 1, o campo 94 do registro 10 somente poderá conter os valores 0 ou 1.";
    public static final String EDUCATIONAL_DATA_COMPLEMENTARY_ACTIVITY=
            PROPERTY + "10.94 - Atividade complementar.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não oferece\n1 – Não exclusivamente(true)\n2 – Exclusivamente\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1. Se informado o valor 2, o campo 93" +
                    " deve er igual a 0 e os campos de 95 a 118 do registro 10 devem ser nulos. Se informado o valor" +
                    " 1, o campo 93 do registro 10 somente poderá conter o valor 0 ou 1.";

    public static final String CLASSROOM = "Classroom";
    public static final String CLASSROOM_DESCRIPTION = "Details about the Classroom";
    public static final String CLASSROOM_REGISTER_TYPE=
            PROPERTY + "20.01 - Tipo de registro.\n" +
                    ROLE + "O preenchimento é obrigatório e exclusivo para as escolas em atividade.\n" +
                    "Valor válido: 20.\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1." +
                    " Deverá ser preenchido com o valor 20.";
    public static final String CLASSROOM_UNIT_CODE_INEP=
            PROPERTY + "20.02 - Código de escola – Inep.\n" +
                    ROLE + "O código deverá ser de uma entidade válida, existente no cadastro do Inep e ativa." +
                    " A escola informada deve estar entre as escolas abrangidas pelo perfil do informante.\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1. Código atribuído pelo" +
                    " Inep à escola. Deverá ser o mesmo número informado no campo 2 do registro 00 antecedente.";
    public static final String CLASSROOM_CODE_INEP=
            PROPERTY + "20.03 - Código da Turma - INEP.\n" +
                    ROLE + "Código atribuído pelo Inep à turma.\n" +
                    CONDITION + "Código atribuído pelo Inep à Turma. O campo não deverá ter informação.";
    public static final String CLASSROOM_CODE_UNIT=
            PROPERTY + "20.04 - Código da Turma na Entidade/Escola\n" +
                    ROLE + "Código atribuído pelo Inep à turma.\n" +
                    CONDITION + "Código atribuído pelo Inep à Turma. O campo não deverá ter informação.";
    public static final String CLASSROOM_NAME=
            PROPERTY + "20.05 - Código da Turma na Entidade/Escola.\n" +
                    ROLE + "Código atribuído à turma pelo sistema próprio do usuário migrador.\n";
    public static final String CLASSROOM_START_TIME=
            PROPERTY + "20.06 - Horário inicial.\n" +
                    ROLE + "Campo não obrigatório.";
    public static final String CLASSROOM_END_TIME=
            PROPERTY + "20.08 - Horário final.\n" +
                    ROLE + "Campo não obrigatório.";
    public static final String CLASSROOM_CLASS_ON_SUNDAY=
            PROPERTY + "20.10 - Há aulas aos domingos.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String CLASSROOM_CLASS_ON_MONDAY=
            PROPERTY + "20.11 -  Há aulas às segundas.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String CLASSROOM_CLASS_ON_TUESDAY=
            PROPERTY + "20.12 -  Há aulas às terças.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String CLASSROOM_CLASS_ON_WEDNESDAY=
            PROPERTY + "20.13 - Há aulas às quartas.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String CLASSROOM_CLASS_ON_THURSDAY=
            PROPERTY + "20.14 - Há aulas às quintas.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String CLASSROOM_CLASS_ON_FRIDAY=
            PROPERTY + "20.15 - Há aulas às sexta.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String CLASSROOM_CLASS_ON_SATURDAY=
            PROPERTY + "20.16 - Há aulas aos sábados.\n" +
                    ROLE + "Campo não obrigatório. 0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String CLASSROOM_CLASS_SERVICE_TIPE=
            PROPERTY + "20.17 - Enterra.\n" +
                    ROLE + "Valores permitidos:\n" +
                    "0 – Não se aplica\n" +
                    "1 – Classe hospitalar\n" +
                    "2 – Unidade de internação socioeducativa\n" +
                    "3 – Unidade prisional\n" +
                    "4 – Atividade complementar\n" +
                    "5 – Atendimento educacional especializado (AEE) 94 (complementaryActivity) do registro 10 deverá" +
                    " ser igual a 1 ou 2." +
                    CONDITION + " Se o valor informado for igual a 5 o campo 93 (specializedEducationalService) do registro" +
                    " 10 deverá ser igual a 1 ou 2." +
                    " Se informado o valor 2 o campo 37 (etapa) não pode ter os valores (1,2,3,56)";
    public static final String CLASSROOM_PARTICIPATING_INNOVATIVE_HIGH_SCHOOL_MORE_EDUCATION=
            PROPERTY + "20.18 - Enterra.\n" +
                    ROLE + "Obrigatório para turmas que não informaram o tipo de atendimento igual a \"Classe Hospitalar\"" +
                    " ou \"Atendimento Educacional Especializado\". Apenas escolas públicas podem informar o campo." +
                    " Valores permitidos:\n" +
                    "0 – Não (false)\n1 – Sim (true)\n" +
                    CONDITION + "O Campo operatingSituation (00.03) deve ser igual a 1.";
    public static final String CLASSROOM_ACTIVITY_CODE=
            PROPERTY + "20.19-24 - Código do Tipo de Atividade\n" +
                    ROLE + "Obrigatório para a turma de atividade complementar e pelo menos um tipo de atividade " +
                    "deverá ser preenchido. O mesmo código não deve ser informado mais de uma vez para os tipos de" +
                    " atividade complementar. O valor preenchido deve estar de acordo com a “Tabela de Tipo de" +
                    " Atividade Complementar”.\n" +
                    CONDITION + "O campo 17 deve ter valor informado igual a 4. Pelo menos um dos campos de 19 a 24 deve " +
                    "ser preenchido. O mesmo valor não pode aparecer em mais de um dos campos de 19 a 24." +
                    " O valor preenchido deve estar de acordo com a “Tabela de Tipo de Atividade Complementar”.";
    public static final String CLASSROOM_ACTIVITY=
            PROPERTY + "20.19-24 - Código do Tipo de Atividade\n" +
                    ROLE + "Obrigatório para a turma de atividade complementar e pelo menos um tipo de atividade " +
                    "deverá ser preenchido. O mesmo código não deve ser informado mais de uma vez para os tipos de" +
                    " atividade complementar. O valor preenchido deve estar de acordo com a “Tabela de Tipo de" +
                    " Atividade Complementar”.\n" +
                    CONDITION + "O campo 17 deve ter valor informado igual a 4. Pelo menos um dos campos de 19 a 24 deve " +
                    "ser preenchido. O mesmo valor não pode aparecer em mais de um dos campos de 19 a 24." +
                    " O valor preenchido deve estar de acordo com a “Tabela de Tipo de Atividade Complementar”.";
    public static final String CLASSROOM_MODALITY=
            PROPERTY + "20.19-24 - Código do Tipo de Atividade complementar ou atendimento educacional " +
                    "especializado. Valores permitidos:\n" +
                    "1 - Ensino Regular\n" +
                    "2 - Educação Especial - Modalidade Substitutiva\n" +
                    "3 - Educação de Jovens e Adultos (EJA).\n" +
                    CONDITION + "O campo 17 deve ser diferente de 4 ou 5. Se informado o valor igual a 1, o campo 95 " +
                    "do registro 10 deve ser igual a 1. Se informado o valor igual a 2, o campo 96 " +
                    "do registro 10 deve ser igual a 1. Se informado o valor igual a 3, o campo 97 " +
                    "do registro 10 deve ser igual a 1.";
    public static final String CLASSROOM_TEACHING_STAGE=
            PROPERTY + "20.37 - Etapa de Ensino (*)\n" +
                    ROLE + "Obrigatório para a turma que não é de atividade complementar ou atendimento educacional " +
                    "especializado. Deverá ser preenchido de acordo com a “Tabela de Etapas x Modalidades de Ensino”." +
                    "Se a turma for em unidade de internação ou unidade prisional, não pode ser informada a etapa " +
                    "Educação Infantil - Creche, Educação Infantil - Pré-escola, Educação Infantil – Unificada, ou " +
                    "Educação Infantil e Ensino Fundamental (8 e 9 anos) Multietapa.\n" +
                    CONDITION + "O campo 17 deve ser diferente de 4 ou 5. O valor informado deve estar entre os valores " +
                    "possíveis (de acordo com a “Tabela de Etapas x Modalidades de Ensino”) para as etapas com" +
                    " valor igual a 1 nos campos de 98 a 118 do registro 10. Se o valor do campo 17 for igual a 2 ou" +
                    " 3 não pode ser informado a etapa 1, 2, 3 ou 56.";
    public static final String CLASSROOM_CODE_COURSE_VOCATIONAL_EDUCATION=
            PROPERTY + "20.38 - Código Curso Educação Profissional (*)\n" +
                    ROLE + "Obrigatório para a turma de educação profissional. Deverá ser preenchido de acordo com a “Tabela " +
                    "de Cursos da Educação Profissional”.\n" +
                    CONDITION + "O campo 37 deve ser igual a 30, 31, 32, 33, 34, 39, 40, 62, 63 ou 64. O valor informado deve" +
                    " estar dentre os valores da “Tabela de Cursos da Educação Profissional”";
    public static final String CLASSROOM_UNIT_SUBJECTS=
            PROPERTY + "20.39-64 - Disciplina(*)\n" +
                    ROLE + "Não Deverá ser preenchido quando a turma for de atividade complementar, ou de atendimento " +
                    "educacional especializado, ou a etapa da turma for de educação infantil (creche, pré-escola," +
                    " educação infantil unificada) ou Projovem(urbano). As disciplinas informadas devem estar" +
                    " dentre as possíveis para a etapa correspondente, conforme a “Tabela de Regras de Disciplinas”. " +
                    "Valores permitidos:\n" +
                    "0 – Não oferece disciplina\n" +
                    "1 – Sim, oferece disciplina com docente vinculado.\n" +
                    "2 – Sim, oferece disciplina sem docente\n" +
                    "vinculado.\n" +
                    CONDITION + "Se o campo 17 for igual a 4 ou 5, ou o campo 37 for igual a 1, 2, 3, 65 ou 66, os " +
                    "campos de 39 a 64 não deverão ser informados. Para as demais ocorrências os campos " +
                    "devem ser preenchidos com um dos valores possíveis (0,1,2). As disciplinas informadas devem" +
                    " estar dentre as possíveis para a etapa correspondente, conforme a" +
                    " “Tabela de Regras de Disciplinas”.";
    public static final String CLASSROOM_UNIT_CLASSROOM_WITHOUT_SCHOOL_PROFESSIONAL=
            PROPERTY + "20.39-64 - Disciplina(*)\n" +
                    ROLE + "Não Deverá ser preenchido quando a turma for de atividade complementar, ou de atendimento " +
                    "educacional especializado, ou a etapa da turma for de educação infantil (creche, pré-escola," +
                    " educação infantil unificada) ou Projovem(urbano). As disciplinas informadas devem estar" +
                    " dentre as possíveis para a etapa correspondente, conforme a “Tabela de Regras de Disciplinas”. " +
                    "Valores permitidos:\n" +
                    "0 – Não oferece disciplina\n" +
                    "1 – Sim, oferece disciplina com docente vinculado.\n" +
                    "2 – Sim, oferece disciplina sem docente\n" +
                    "vinculado.\n" +
                    CONDITION + "Se o campo 17 for igual a 4 ou 5, ou o campo 37 for igual a 1, 2, 3, 65 ou 66, os " +
                    "campos de 39 a 64 não deverão ser informados. Para as demais ocorrências os campos " +
                    "devem ser preenchidos com um dos valores possíveis (0,1,2). As disciplinas informadas devem" +
                    " estar dentre as possíveis para a etapa correspondente, conforme a" +
                    " “Tabela de Regras de Disciplinas”.";

    /* ********** AdditionalActivityType ********** */
    public static final String ADDITIONAL_ACTIVITY_TYPE = "Additional activity type";
    public static final String ADDITIONAL_ACTIVITY_TYPE_DESCRIPTION = "Details about the additional activity type";
    public static final String ADDITIONAL_ACTIVITY_TYPE_ACTIVITY_CODE=
            PROPERTY + "20.19-24 - Specialized Educational Care Activities";

    /* ********** SpecializedEducationalCareActivities ********** */
    public static final String SPECIALIZED_EDUCATIONAL_CARE_ACTIVITIES = "Specialized Educational Care Activities";
    public static final String SPECIALIZED_EDUCATIONAL_CARE_ACTIVITIES_DESCRIPTION =
            "Details about specialized educational care activities";
    public static final String SPECIALIZED_EDUCATIONAL_CARE_ACTIVITIES_BRAILLE_SYSTEM_TEACHING=
            PROPERTY + "20.19-24 - Código do Tipo de Atividade\n";
    public static final String SPECIALIZED_EDUCATIONAL_CARE_ACTIVITIES_RESOURCE_USE_TEACHING_OPTICAL_AND_NONOPTICAL=
            PROPERTY + "20.19-24 - Código do Tipo de Atividade\n";
    public static final String SPECIALIZED_EDUCATIONAL_CARE_ACTIVITIES_STRATEGIES_FOR_THE_DEVELOPMENT_OF_MENTAL_PROCESSES=
            PROPERTY + "20.19-24 - Código do Tipo de Atividade\n";
    public static final String SPECIALIZED_EDUCATIONAL_CARE_ACTIVITIES_ORIENTATION_AND_MOBILITY_TECHNIQUES=
            PROPERTY + "20.19-24 - Código do Tipo de Atividade\n";
    public static final String SPECIALIZED_EDUCATIONAL_CARE_ACTIVITIES_BRAZILIAN_SIGN_LANGUAGE_TEACHING=
            PROPERTY + "20.19-24 - Código do Tipo de Atividade\n";
    public static final String SPECIALIZED_EDUCATIONAL_CARE_ACTIVITIES_ALTERNATIVE_AND_AUGMENTATIVE_COMMUNICATION=
            PROPERTY + "20.19-24 - Código do Tipo de Atividade\n";
    public static final String SPECIALIZED_EDUCATIONAL_CARE_ACTIVITIES_STRATEGIES_FOR_CURRICULUM_ENRICHMENT=
            PROPERTY + "20.19-24 - Código do Tipo de Atividade\n";
    public static final String SPECIALIZED_EDUCATIONAL_CARE_ACTIVITIES_TEACHING_THE_USE_OF_SOROBAN=
            PROPERTY + "20.19-24 - Código do Tipo de Atividade\n";
    public static final String SPECIALIZED_EDUCATIONAL_CARE_ACTIVITIES_TEACH_THE_USABILITY_AND_FUNC_OF_ACCESSIBLE_COMPUTING=
            PROPERTY + "20.19-24 - Código do Tipo de Atividade\n";
    public static final String SPECIALIZED_EDUCATIONAL_CARE_ACTIVITIES_PORTUGUESE_LANGUAGE_TEACH_WRITTEN_MODALITY=
            PROPERTY + "20.19-24 - Código do Tipo de Atividade\n";
    public static final String SPECIALIZED_EDUCATIONAL_CARE_ACTIVITIES_STRATEGIES_FOR_AUTONOMY_IN_THE_SCHOOL_ENVIRONMENT=
            PROPERTY + "20.19-24 - Código do Tipo de Atividade\n";

    /* ********** UnitSubjects ********** */
    public static final String UNIT_SUBJECT = "Unit Subjects";
    public static final String UNIT_SUBJECT_CLASS_DESCRIPTION = "Details about unit subjects";
    public static final String UNIT_SUBJECT_TYPE=
            PROPERTY + "20.39-64 - Nome da disciplina.\n";
    public static final String UNIT_SUBJECT_NAME=
            PROPERTY + "20.39-64 - Nome da disciplina.\n";
    public static final String UNIT_SUBJECT_FIELD_DESCRIPTION=
            PROPERTY + "20.39-64 - Nome da disciplina.\n";
}
