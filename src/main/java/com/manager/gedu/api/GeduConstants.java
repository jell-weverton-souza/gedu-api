package com.manager.gedu.api;

/**
 * @author Weverton Souza.
 * Created on 14/07/19
 */
public class GeduConstants {

    /* ********** RESOURCES OPERATIONS ********** */
    public static final String RESOURCE_OPERATION_CREATE = ">> Create <<";
    public static final String RESOURCE_OPERATION_UPDATE = ">> Update <<";
    public static final String RESOURCE_OPERATION_FIND_BY_ID = ">> Find by id <<";
    public static final String RESOURCE_OPERATION_FIND_ALL = ">> Find all <<";
    public static final String RESOURCE_OPERATION_DELETE = ">> Delete <<";

    /* ********** ADDRESS ********** */
    public static final String BASE_URL_ADDRESS_NAME = "addresses";
    public static final String BASE_URL_ADDRESS = "/" + BASE_URL_ADDRESS_NAME;
    public static final String BASE_URL_ADDRESS_TAG = "Address";
    public static final String BASE_URL_ADDRESS_ZIP_CODE = "/zipcode/{zipcode}";
    public static final String ADDRESS_OPERATION_ZIP_CODE = "Location by zipcode.";

    /* ********** Unit ********** */
    public static final String BASE_URL_UNIT_NAME = "units";
    public static final String BASE_URL_UNIT = "/" + BASE_URL_UNIT_NAME;
    public static final String BASE_URL_UNIT_TAG = "Unit";

    /* ********** COURSE ********** */
    public static final String BASE_URL_COURSE_NAME = "courses";
    public static final String BASE_URL_COURSE = "/" + BASE_URL_COURSE_NAME;
    public static final String BASE_URL_COURSE_TAG = "Course";

    /* ********** USER ********** */
    public static final String BASE_URL_USER_NAME = "users";
    public static final String BASE_URL_USER = "/" + BASE_URL_USER_NAME;
    public static final String BASE_URL_USER_TAG = "User";

    /* ********** USER ********** */
    public static final String BASE_URL_STAFF_NAME = "staff";
    public static final String BASE_URL_STAFF = "/" + BASE_URL_STAFF_NAME;
    public static final String BASE_URL_STAFF_TAG = "Staff";

    /* ********** PERSON ********** */
    public static final String BASE_URL_PERSON_NAME = "person";
    public static final String BASE_URL_PERSON = "/" + BASE_URL_PERSON_NAME;
    public static final String BASE_URL_PERSON_TAG = "Person";

    /* ********** USERACCESSES ********** */
    public static final String BASE_URL_USER_ACCESS_NAME = "users-accesses";
    public static final String BASE_URL_USER_ACCESS = "/" + BASE_URL_PERSON_NAME;
    public static final String BASE_URL_USER_ACCESS_TAG = "User Access";

    /* ********** UnitMaintainer ********** */
    public static final String BASE_URL_UNIT_MAINTAINER_NAME = "unit-maintainers";
    public static final String BASE_URL_UNIT_MAINTAINER = "/" + BASE_URL_UNIT_MAINTAINER_NAME;
    public static final String BASE_URL_UNIT_MAINTAINER_TAG = "Unit Maintainer";

    /* ********** InfrastructureUnit ********** */
    public static final String BASE_URL_INFRASTRUCTURE_UNIT_NAME = "infrastructure-units";
    public static final String BASE_URL_INFRASTRUCTURE_UNIT = "/" + BASE_URL_INFRASTRUCTURE_UNIT_NAME;
    public static final String BASE_URL_INFRASTRUCTURE_UNIT_TAG = "InfrastructureUnit";

    /* ********** CLASSES ********** */
    public static final String BASE_URL_CLASSROOM_NAME = "classes";
    public static final String BASE_URL_CLASSROOM = "/" + BASE_URL_CLASSROOM_NAME;
    public static final String BASE_URL_CLASSROOM_TAG = "Class";
    public static final String CLASSROOM_OPERATION_SERVICE_TYPE = "Fetch the types of services";
    public static final String CLASSROOM_OPERATION_OFFERS_SUBJECTS = "Fetch the subjects offered.";

    // ********** REST Constants Message **********
    public static final String HTTP_MESSAGE_200 = "The request has succeeded.";
    public static final String HTTP_MESSAGE_201 = "The request has been fulfilled and resulted in a new resource being created.";
    public static final String HTTP_MESSAGE_401 = "The request requires user authentication.";
    public static final String HTTP_MESSAGE_403 = "The server understood the request, but is refusing to fulfill it.";
    public static final String HTTP_MESSAGE_404 = "The server has not found anything matching the Request-URI.";
}
