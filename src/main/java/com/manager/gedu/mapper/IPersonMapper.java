package com.manager.gedu.mapper;

import com.manager.gedu.datatransferobject.user.PersonDTO;
import com.manager.gedu.domain.user.Person;
import com.manager.gedu.interfaces.IDomainMapper;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @author Weverton Souza.
 * Created on 25/08/2019
 */

@Mapper
public interface IPersonMapper extends IDomainMapper<Person, PersonDTO> {
    @Override
    PersonDTO toDTO(final Person domain);
    @Override
    Person toDomain(final PersonDTO dto);
    @Override
    List<PersonDTO> toPageDTO(final List<Person> items);
}
