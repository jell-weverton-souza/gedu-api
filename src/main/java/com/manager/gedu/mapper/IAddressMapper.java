package com.manager.gedu.mapper;

import com.manager.gedu.datatransferobject.address.AddressDTO;
import com.manager.gedu.domain.address.Address;
import com.manager.gedu.interfaces.IDomainMapper;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @author Weverton Souza.
 * Created on 26/07/2019
 */
@Mapper
public interface IAddressMapper extends IDomainMapper<Address, AddressDTO> {
    @Override
    AddressDTO toDTO(final Address domain);
    @Override
    Address toDomain(final AddressDTO dto);
    @Override
    List<AddressDTO> toPageDTO(final List<Address> items);
}
