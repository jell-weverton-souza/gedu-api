package com.manager.gedu.mapper;

import com.manager.gedu.datatransferobject.gradesubject.GradeDTO;
import com.manager.gedu.domain.gradesubject.Grade;
import com.manager.gedu.interfaces.IDomainMapper;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface IGradeMapper extends IDomainMapper<Grade, GradeDTO> {
    @Override
    GradeDTO toDTO(final Grade domain);
    @Override
    Grade toDomain(final GradeDTO dto);
    @Override
    List<GradeDTO> toPageDTO(final List<Grade> items);
}
