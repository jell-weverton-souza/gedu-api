package com.manager.gedu.mapper;

import com.manager.gedu.datatransferobject.user.UserAccessDTO;
import com.manager.gedu.domain.user.UserAccess;
import com.manager.gedu.interfaces.IDomainMapper;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @author Weverton Souza.
 * Created on 25/08/2019
 */
@Mapper
public interface IUserAccessMapper extends IDomainMapper<UserAccess, UserAccessDTO> {
    @Override
    UserAccessDTO toDTO(final UserAccess domain);
    @Override
    UserAccess toDomain(final UserAccessDTO dto);
    @Override
    List<UserAccessDTO> toPageDTO(final List<UserAccess> items);
}
