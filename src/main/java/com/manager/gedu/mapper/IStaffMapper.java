package com.manager.gedu.mapper;

import com.manager.gedu.datatransferobject.staff.StaffDTO;
import com.manager.gedu.domain.staff.Staff;
import com.manager.gedu.interfaces.IDomainMapper;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @author Weverton Souza.
 * Created on 28/06/19
 */
@Mapper
public interface IStaffMapper extends IDomainMapper<Staff, StaffDTO> {
    @Override
    StaffDTO toDTO(final Staff domain);
    @Override
    Staff toDomain(final StaffDTO dto);
    @Override
    List<StaffDTO> toPageDTO(final List<Staff> items);
}

