package com.manager.gedu.mapper;

import com.manager.gedu.datatransferobject.user.UserDTO;
import com.manager.gedu.domain.user.User;
import com.manager.gedu.interfaces.IDomainMapper;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @author Weverton Souza.
 * Created on 25/08/2019
 */
@Mapper
public interface IUserMapper extends IDomainMapper<User, UserDTO> {
    @Override
    UserDTO toDTO(final User domain);
    @Override
    User toDomain(final UserDTO dto);
    @Override
    List<UserDTO> toPageDTO(final List<User> items);
}
