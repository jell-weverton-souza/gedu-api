package com.manager.gedu.mapper;

import com.manager.gedu.datatransferobject.student.StudentDTO;
import com.manager.gedu.domain.student.Student;
import com.manager.gedu.interfaces.IDomainMapper;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface IStudentMapper extends IDomainMapper<Student, StudentDTO> {
    @Override
    StudentDTO toDTO(final Student domain);
    @Override
    Student toDomain(final StudentDTO dto);
    @Override
    List<StudentDTO> toPageDTO(final List<Student> items);
}
