package com.manager.gedu.mapper;

import com.manager.gedu.datatransferobject.task.ScoreDTO;
import com.manager.gedu.domain.task.Score;
import com.manager.gedu.interfaces.IDomainMapper;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface IScoreMapper extends IDomainMapper<Score, ScoreDTO> {
    @Override
    ScoreDTO toDTO(final Score domain);
    @Override
    Score toDomain(final ScoreDTO dto);
    @Override
    List<ScoreDTO> toPageDTO(final List<Score> items);
}
