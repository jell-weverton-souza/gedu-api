package com.manager.gedu.domain;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author Weverton Souza.
 * Created on 21/07/19
 */
public class UnitEquipmentTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void setID() {
    }

    @Test
    public void getID() {
    }

    @Test
    public void setCreateBy() {
    }

    @Test
    public void setUpdateBy() {
    }

    @Test
    public void setTimestamp() {
    }

    @Test
    public void getHasTelevisionDevice() {
    }

    @Test
    public void setHasTelevisionDevice() {
    }

    @Test
    public void getHasVCR() {
    }

    @Test
    public void setHasVCR() {
    }

    @Test
    public void getHasParabolicAntenna() {
    }

    @Test
    public void setHasParabolicAntenna() {
    }

    @Test
    public void getHasCopyMachine() {
    }

    @Test
    public void setHasCopyMachine() {
    }

    @Test
    public void getHasOverheadProjector() {
    }

    @Test
    public void setHasOverheadProjector() {
    }

    @Test
    public void getHasPrinter() {
    }

    @Test
    public void setHasPrinter() {
    }

    @Test
    public void getHasStereoSystem() {
    }

    @Test
    public void setHasStereoSystem() {
    }

    @Test
    public void getHasDataShow() {
    }

    @Test
    public void setHasDataShow() {
    }

    @Test
    public void getHasFax() {
    }

    @Test
    public void setHasFax() {
    }

    @Test
    public void getHasCamera() {
    }

    @Test
    public void setHasCamera() {
    }

    @Test
    public void getHasComputers() {
    }

    @Test
    public void setHasComputers() {
    }

    @Test
    public void getNumberOfComputersOfAdministrativeUse() {
    }

    @Test
    public void setNumberOfComputersOfAdministrativeUse() {
    }

    @Test
    public void getNumberOfComputersOfStudentUse() {
    }

    @Test
    public void setNumberOfComputersOfStudentUse() {
    }

    @Test
    public void getHasInternetAccess() {
    }

    @Test
    public void setHasInternetAccess() {
    }

    @Test
    public void getHasBroadband() {
    }

    @Test
    public void setHasBroadband() {
    }
}